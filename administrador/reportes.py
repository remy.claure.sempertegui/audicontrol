from .basejasperreport import BaseJasperReport

class ReporteCostos(BaseJasperReport):
    report_name = ''
    xparametro1 = ''
    xmes = ''
    xmes_str = ''
    xpersonal_id=''
    xanio = ''

    def __init__(self, classroom, xnombre , xparametro1, xmes, xmes_str, xpersonal_id, xanio):
        self.classroom = classroom
        self.filename = 'reporte_' + xnombre + '_{}'.format('-')
        self.report_name = xnombre
        self.xparametro1 = xparametro1
        self.xmes = xmes
        self.xmes_str = xmes_str
        self.xpersonal_id=xpersonal_id
        self.xanio = xanio

        
        super(ReporteCostos, self).__init__()
    def get_params(self):
        return {
            'xparametro1'  : self.xparametro1,
            'xmes'         : self.xmes, 
            'xmes_str'     : self.xmes_str,
            'xpersonal_id' : self.xpersonal_id,
            'xanio'        : self.xanio
        }

class ReporteMensual(BaseJasperReport):
    report_name = 'reportcontroldiariohoras'
    xmes = ''
    xmes_str = ''
    xpersonal_id=''
    xanio = ''
    xproyecto_id=''
    def __init__(self, classroom, xmes, xmes_str, xpersonal_id, xanio, xproyecto_id):
        self.classroom = classroom
        self.filename = 'reporte_' + self.report_name + '_'+str(xmes)+'_'+xpersonal_id+'_{}'.format('-')
        self.xmes = xmes
        self.xmes_str = xmes_str
        self.xpersonal_id=xpersonal_id        
        self.xanio = xanio        
        self.xproyecto_id=xproyecto_id
        super(ReporteMensual, self).__init__()
    def get_params(self):
        return {
            'xmes'         : self.xmes, 
            'xmes_str'     : self.xmes_str,
            'xpersonal_id' : self.xpersonal_id,
            'xanio'        : self.xanio,
            'xproyecto_id' : self.xproyecto_id
        }

class ReporteMensualAuditor(BaseJasperReport):
    report_name = 'reportmensualauditor'
    xmes = ''
    xmes_str = ''
    xpersonal_id=''
    xanio = ''
    def __init__(self, classroom, xmes, xmes_str, xpersonal_id, xanio):
        self.classroom = classroom
        self.filename = 'reporte_' + self.report_name + '_'+str(xmes)+'_'+str(xpersonal_id)+'_{}'.format('-')
        self.xmes = xmes
        self.xmes_str = xmes_str
        self.xpersonal_id=xpersonal_id
        self.xanio = xanio        
        super(ReporteMensualAuditor, self).__init__()
    def get_params(self):
        return {
            'xmes'         : self.xmes, 
            'xmes_str'     : self.xmes_str,
            'xpersonal_id' : self.xpersonal_id,
            'xanio'        : self.xanio,
        }

class ReporteAnualAuditor(BaseJasperReport):
    report_name = 'reportcontrolmensualhoras'
    xpersonal_id=''
    xanio = ''

    def __init__(self, classroom, xpersonal_id, xanio, xproyecto_id):
        self.classroom = classroom
        self.filename = 'reporte_' + self.report_name + '_'+str(xpersonal_id)+'_{}'.format('-')
        self.xpersonal_id=xpersonal_id
        self.xanio = xanio        
        self.xproyecto_id=xproyecto_id
        super(ReporteAnualAuditor, self).__init__()
    def get_params(self):
        return {
            'xpersonal_id' : self.xpersonal_id,
            'xanio'        : self.xanio,
            'xproyecto_id' : self.xproyecto_id,
        }

class ReporteMensualSupervisor(BaseJasperReport):
    report_name = 'reportcontroldiariosupervisor'
    def __init__(self, classroom, xmes, xgestion, xmes_str, supervisor_id):
        self.classroom = classroom
        self.filename = 'reporte_' + self.report_name + '_'+str(supervisor_id)+'_{}'.format('-')
        self.xmes=xmes
        self.xgestion = xgestion                
        self.xmes_str=xmes_str
        self.supervisor_id=supervisor_id
        super(ReporteMensualSupervisor, self).__init__()
    def get_params(self):
        return {
            'xmes'        : self.xmes,
            'xgestion' : self.xgestion,            
            'xmes_str'        : self.xmes_str,
            'supervisor_id' : self.supervisor_id,
        }

class ReporteResumenProyecto(BaseJasperReport):
    report_name = 'reportresumenproyecto'
    def __init__(self, classroom, xproyecto_id):
        self.classroom = classroom
        self.filename = 'reporte_' + self.report_name + '_'+str(xproyecto_id)+'_{}'.format('-')
        self.xproyecto_id=xproyecto_id        
        super(ReporteResumenProyecto, self).__init__()
    def get_params(self):
        return {
            'xproyecto_id'        : self.xproyecto_id            
        }

class ReporteResumenTrabajadorAuditor(BaseJasperReport):
    report_name = 'reportresumendeltrabajoauditor'
    def __init__(self, classroom, xgestion, xsupervisor_id):
        self.classroom = classroom
        self.filename = 'reporte_' + self.report_name + '_{}'.format('-')
        self.xgestion=xgestion
        self.xsupervisor_id=xsupervisor_id

        super(ReporteResumenTrabajadorAuditor, self).__init__()
    def get_params(self):
        return {
            'xgestion'        : self.xgestion, 
            'xsupervisor_id'  : self.xsupervisor_id                        
        }

class ReporteResumenTrabajadorAuditor3(BaseJasperReport):
    report_name = 'reportresumendeltrabajoauditor_1'
    def __init__(self, classroom, xgestion, xsupervisor_id):
        self.classroom = classroom
        self.filename = 'reporte_' + self.report_name + '_{}'.format('-')
        self.xgestion=xgestion
        self.xsupervisor_id=xsupervisor_id

        super(ReporteResumenTrabajadorAuditor3, self).__init__()
    def get_params(self):
        return {
            'xgestion'        : self.xgestion, 
            'xsupervisor_id'  : self.xsupervisor_id                        
        }

class ReporteControlDiarioDirector(BaseJasperReport):
    report_name = 'reportcontroldiariodirector'
    def __init__(self, classroom, xmes, xgestion, xmes_str):
        self.classroom = classroom
        self.filename = 'reporte_' + self.report_name + '_{}'.format('-')
        self.xmes=xmes
        self.xgestion=xgestion
        self.xmes_str=xmes_str
        

        super(ReporteControlDiarioDirector, self).__init__()
    def get_params(self):
        return {
            'xmes'        : self.xmes, 
            'xgestion'    : self.xgestion, 
            'xmes_str'    : self.xmes_str                        
        }

class ReporteRelacionCargos(BaseJasperReport):
    report_name = 'reportrelacioncargos'
    def __init__(self, classroom, GESTION):
        self.classroom = classroom
        self.filename = 'reporte_' + self.report_name + '_{}'.format('-')
        self.GESTION=GESTION
        super(ReporteRelacionCargos, self).__init__()
    def get_params(self):
        return {
            'GESTION'    : self.GESTION, 
        }

class ReporteActualizacionAuditor(BaseJasperReport):
    report_name = 'reportactualizacionauditor'
    def __init__(self, classroom, xid_personal):
        self.classroom = classroom
        self.filename = 'reporte_' + self.report_name + '_{}'.format('-')
        self.xid_personal=xid_personal
        super(ReporteActualizacionAuditor, self).__init__()
    def get_params(self):
        return {
            'xid_personal'    : self.xid_personal, 
        }

class ReportePoaAudi(BaseJasperReport):
    report_name = 'reportpoaaudi'
    def __init__(self, classroom, xgestion):
        self.classroom = classroom
        self.filename = 'reporte_' + self.report_name + '_{}'.format('-')
        self.xgestion=xgestion
        super(ReportePoaAudi, self).__init__()
    def get_params(self):
        return {
            'xgestion'    : self.xgestion, 
        }