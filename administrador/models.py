from django.db import models
from django.contrib.auth.models import User

# Create your models here.
TITULO_CHOICES = (
    ('LIC.', 'LICENCIADO'),
    ('ABOG.', 'ABOGADO'),
    ('ING.', 'INGENIERO'),
)
DEPENDENCIA_CHOICE = (
    ('Gobernador', 'Gobernador'),
    ('Director', 'Director'),
    ('Supervisor', 'Supervisor'),
    ('Responsable de unidad', 'Responsable de unidad'),
)
TIPO_CHOICES = (
    ('AP', 'ACTIVIDAD PROGRAMADA'),
    ('ANP', 'ACTIVIDAD NO PROGRAMADA'),
)
class Mes(models.Model):
    mes = models.IntegerField()

class TiempoDisponible(models.Model):
    mes = models.ForeignKey(Mes, null=True, blank=True, on_delete=models.CASCADE)
    sabados_domingos = models.IntegerField()
    feriados = models.IntegerField()
    dias_totales = models.IntegerField()
    dias_habiles = models.IntegerField()
    gestion = models.IntegerField()

class Institucion(models.Model):
    nombre_institucion = models.CharField(max_length=50)
    
    def __str__(self):
        return '{}'.format(self.nombre_institucion)

class TipoAuditor(models.Model):
    nombre_tipoauditor = models.CharField(max_length=15)
    institucion = models.ForeignKey(Institucion, null=True, blank=True, on_delete=models.CASCADE)
    def __str__(self):
        return '{}'.format(self.nombre_tipoauditor)

class Profesion(models.Model):
    nombre_profesion = models.CharField(max_length=50)
    titulo =models.CharField(max_length=50)
    def __str__(self):
        return '{}'.format(self.nombre_profesion)

class Personal(models.Model):
    ci = models.CharField(max_length=15)
    nombre = models.CharField(max_length=25)
    a_paterno = models.CharField(max_length=25)
    a_materno = models.CharField(max_length=25)
    usuario = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)    
    sexo = models.CharField(max_length=5)
    profesion = models.ForeignKey(Profesion, null=True, blank=True, on_delete=models.CASCADE)
    cargo =  models.CharField(max_length=65)
    experiencia_sector_publico_anios = models.IntegerField()
    experiencia_sector_publico_meses = models.IntegerField()
    experiencia_sector_privado_anios = models.IntegerField()
    experiencia_sector_privado_meses = models.IntegerField()
    experiencia_auditor_sector_publico_anios = models.IntegerField()
    experiencia_auditor_sector_publico_meses = models.IntegerField()
    experiencia_auditor_sector_privado_anios = models.IntegerField()
    experiencia_auditor_sector_privado_meses = models.IntegerField()
    fecha_incorporacion_entidad = models.DateField()
    fecha_incorporacion_unidad = models.DateField()
    remuneracion_anual = models.FloatField()
    titulo_prov_nal = models.CharField(max_length=15) 
    nro_registro_deptal = models.CharField(max_length=15) 
    nro_registro_nacional = models.CharField(max_length=15) 
    dependencia = models.CharField(max_length=15, choices=DEPENDENCIA_CHOICE)  
    tipoauditor = models.ForeignKey(TipoAuditor, null=True, blank=True, on_delete=models.CASCADE)
    item = models.CharField(max_length=15)
    nro_memorandum = models.CharField(max_length=15)

    def trabajosasignados(self):
        asignacion = Asignacion.objects.filter(personal=self)
        if asignacion:
           return asignacion.count()
        else:
             return 0
    def diasasignadas(self):
        asignaciones = Asignacion.objects.filter(personal=self)
        if asignaciones:
           dias = 0 
           for asignacion in asignaciones:
               if asignacion.plazo:
                  dias = dias + asignacion.plazo
           return dias
        else:
             return 0
    def horasasignadas(self):
        asignaciones = Asignacion.objects.filter(personal=self)
        if asignaciones:
           horas=self.diasasignadas()*8
           return horas
        else:
             return 0

    def horashabiles(self):
        horas = self.dias_habiles()*8
        return horas

    def dias_habiles(self):
        tiemposdisponible=PoaPersonal.objects.filter(personal=self, gestion=2021)
        diash = 0
        for tiempodisponible in tiemposdisponible:
            diash = diash + tiempodisponible.dias_habiles
        return diash

    def dias_vacacion(self):        
        tiemposdisponible=PoaPersonal.objects.filter(personal=self, gestion=2021)
        diasv = 0
        for tiempodisponible in tiemposdisponible:
            diasv = diasv + tiempodisponible.dias_vacacion
        return diasv
    
    def dias_lactancia(self):        
        diasl = 0 
        tiemposdisponible=PoaPersonal.objects.filter(personal=self, gestion=2021)
        for tiempodisponible in tiemposdisponible:
            diasl = diasl + tiempodisponible.dias_lactancia       
        return diasl
    
    def dias_capacitacion(self):        
        diasc = 0 
        tiemposdisponible=PoaPersonal.objects.filter(personal=self, gestion=2021)
        for tiempodisponible in tiemposdisponible:
            diasc = diasc + tiempodisponible.dias_capacitacion       
        return diasc
    def dias_administrativos(self):        
        diasa = 0 
        tiemposdisponible=PoaPersonal.objects.filter(personal=self, gestion=2021)
        for tiempodisponible in tiemposdisponible:
            diasa = diasa + tiempodisponible.dias_administrativos       
        return diasa
    
    def dias_efectivos(self):        
        return (self.dias_laborales()-self.dias_administrativos())
    
    def dias_no_prog(self):        
        diasn = 0 
        tiemposdisponible=PoaPersonal.objects.filter(personal=self, gestion=2021)
        for tiempodisponible in tiemposdisponible:
            diasn = diasn + tiempodisponible.dias_no_prog       
        return diasn

    def dias_prog(self):        
        return (self.dias_laborales()-self.dias_administrativos()-self.dias_no_prog())

    def horasejecutadas(self):
        controles=Control.objects.filter(asignacion__personal=self)
        horas = 0
        for control in controles:
            horas = horas + control.horas
        return horas
    
    def horasporcejecutadas(self):
        if self.dias_prog()>0:
            horasprog = self.dias_prog()
            horasejecutadas=self.horasejecutadas()
        else:
            horasprog = 1
            horasejecutadas = 0

        return (horasejecutadas/(horasprog*8)*100)
    
    def dias_laborales(self):
        return (self.dias_habiles()-self.dias_vacacion()-self.dias_lactancia()-self.dias_capacitacion())

    def horasporcasignadas(self):
        asignaciones = Asignacion.objects.filter(personal=self)
        if self.horashabiles():           
           return (self.diasasignadas()/self.dias_prog())*100
        else:
             return 0

    def __str__(self):
        return '{}'.format(self.nombre + ' ' + self.a_paterno + ' ' + self.a_materno)


class PoaPersonal(models.Model):
    gestion = models.IntegerField()
    personal = models.ForeignKey(Personal, null=True, blank=True, on_delete=models.CASCADE )
    sabados_domingos= models.IntegerField()
    dias_feriados= models.IntegerField()
    dias_habiles= models.IntegerField()
    dias_vacacion = models.IntegerField()
    dias_lactancia = models.IntegerField()
    dias_capacitacion = models.IntegerField()
    dias_administrativos = models.IntegerField()
    dias_no_prog = models.IntegerField()
    remuneracion_anual = models.FloatField()
    def dias_laborales(self):
        return (self.dias_habiles-self.dias_vacacion-self.dias_lactancia-self.dias_capacitacion)
    def dias_efectivos(self):
        return (self.dias_laborales()-self.dias_administrativos)
    def dias_prog(self):
        return (self.dias_efectivos()-self.dias_no_prog)



class TipoAuditoria(models.Model):
    nombre_tipoauditoria = models.CharField(max_length=15)    
    def __str__(self):
        return '{}'.format(self.nombre_tipoauditoria)

class Proyecto(models.Model):
    nombre_proyecto = models.CharField(max_length=250)
    fecha_inicio = models.DateField()
    plazo = models.IntegerField()
    tipo = models.CharField(max_length=7, choices=TIPO_CHOICES)
    tipoauditoria = models.ForeignKey(TipoAuditoria, null=True, blank=True, on_delete=models.CASCADE)
    def plazo_asignado(self):
        asignaciones = Asignacion.objects.filter(proyecto=self)
        plazo=0
        if asignaciones:
            for asignacion in asignaciones:
                print(asignacion.plazo)
                if asignacion.plazo: 
                   plazo = plazo + asignacion.plazo 
            return plazo
            
        else:
            return plazo
    def __str__(self):
        return '{}'.format(str(self.id) + ' '+ self.nombre_proyecto)

class TipoAsignacion(models.Model):
    nombre_tipo = models.CharField(max_length=125)
    
    def __str__(self):
        return '{}'.format(self.nombre_tipo)

class Actividad(models.Model):
    nombre_actividad = models.CharField(max_length=125)
    orden = models.IntegerField()
    def __str__(self):
        return '{}'.format(self.id)

class Etapa(models.Model):
    nombre_etapa = models.CharField(max_length=25)
    tiempo_para = models.CharField(max_length=15)
    def __str__(self):
        return '{}'.format(self.id)

class Asignacion(models.Model):
    personal = models.ForeignKey(Personal, null=True, blank=True, on_delete=models.CASCADE)
    proyecto = models.ForeignKey(Proyecto, null=True, blank=True, on_delete=models.CASCADE)
    tipoasignacion = models.ForeignKey(TipoAsignacion, null=True, blank=True, on_delete=models.CASCADE)  
    nro_memorandum = models.CharField(max_length=4)
    gestion_memorandum = models.IntegerField()
    cargo = models.CharField(max_length=150)
    supervisor = models.ForeignKey(Personal, null=True, blank=True, on_delete=models.CASCADE, related_name='supervisor_id')
    plazo = models.IntegerField()  
    tipoauditor = models.ForeignKey(TipoAuditor, null=True, blank=True, on_delete=models.CASCADE, related_name='supervisor_id')
    estado = models.CharField(max_length=15, default='EN ESPERA')
    fecha_inicio = models.DateField()
    fecha_memorandum = models.DateField(null=True, blank=True)
    plazo_poa = models.IntegerField()  
    supervisorpoa = models.ForeignKey(Personal, null=True, blank=True, on_delete=models.CASCADE, related_name='supervisorpoa_id')
    poa = models.BooleanField(default=True)
    memo = models.BooleanField(default=False)
    plazo_supervisor_poa = models.IntegerField()  
    plazo_supervisor_memo = models.IntegerField()  
    numero_memorandum_supervisor = models.CharField(max_length=5)  
    plazo_director = models.IntegerField()  
    gestion = models.IntegerField() 
    fecha_memorandum_supervisor = models.DateField()
    asignacionauditor_id = models.IntegerField()
    asignacionsupervisor_id = models.IntegerField()

    def __str__(self):
        return '{}'.format(self.id)
    class Meta:
        managed = False
        db_table = 'vadministrador_asignacion'    

class AsignacionAuditor(models.Model):
    personal = models.ForeignKey(Personal, null=True, blank=True, on_delete=models.CASCADE)
    cargo = models.CharField(max_length=150)
    plazo = models.IntegerField()  
    tipoauditor = models.ForeignKey(TipoAuditor, null=True, blank=True, on_delete=models.CASCADE, related_name='tipoauditor_id')
    nro_memorandum = models.CharField(max_length=4)
    fecha_memorandum = models.DateField(null=True, blank=True)
    asignacion = models.ForeignKey(Asignacion, null=True, blank=True, on_delete=models.CASCADE)
    estado = models.BooleanField()
    
    def __str__(self):
        return '{}'.format(self.id)
    class Meta:
        managed = False
        db_table = 'administrador_asignacionauditor'   

class AsignacionSupervisor(models.Model):
    supervisor = models.ForeignKey(Personal, null=True, blank=True, on_delete=models.CASCADE, related_name='supervisor_id1')
    plazo_supervisor_poa = models.IntegerField()  
    plazo_supervisor_memo = models.IntegerField()  
    numero_memorandum_supervisor = models.CharField(max_length=5)  
    fecha_memorandum_supervisor = models.DateField()
    supervisorpoa = models.ForeignKey(Personal, null=True, blank=True, on_delete=models.CASCADE, related_name='supervisorpoa_id1')
    asignacion = models.ForeignKey(Asignacion, null=True, blank=True, on_delete=models.CASCADE)
    estado = models.BooleanField()
    
    def __str__(self):
        return '{}'.format(self.id)
    class Meta:
        managed = False
        db_table = 'administrador_asignacionsupervisor'   


class TEA(models.Model):  
    asignacion = models.ForeignKey(Asignacion, null=True, blank=True, on_delete=models.CASCADE)
    actividad = models.ForeignKey(Actividad, null=True, blank=True, on_delete=models.CASCADE)
    etapa = models.ForeignKey(Etapa, null=True, blank=True, on_delete=models.CASCADE) 
    orden = models.IntegerField(null=True, blank=True)
    def __str__(self):
        return '{}'.format(self.id)

class Control(models.Model):  
    asignacion = models.ForeignKey(Asignacion, null=True, blank=True, on_delete=models.CASCADE)
    asignacionauditor = models.ForeignKey(AsignacionAuditor, null=True, blank=True, on_delete=models.CASCADE)
    tea = models.ForeignKey(TEA, null=True, blank=True, on_delete=models.CASCADE)
    fecha = models.DateField()
    horas = models.FloatField()
    estado = models.BooleanField()

    def __str__(self):
        return '{}'.format(self.id)

class CostoAuditoria(models.Model):
    tipoauditor = models.ForeignKey(TipoAuditor, null=True, blank=True, on_delete=models.CASCADE)
    tipoauditoria = models.ForeignKey(TipoAuditoria, null=True, blank=True, on_delete=models.CASCADE)
    dias = models.IntegerField()
    costo_diario = models.FloatField()
    costo_total = models.FloatField()

class PrincipalPersonal(models.Model):
    #id = models.IntegerField(primary_key=True, serial)  # AutoField?
    id = models.AutoField(primary_key=True)
    ci = models.CharField(db_column='CI', max_length=10)  # Field name made lowercase.
    expedido = models.CharField(db_column='expedido', unique=False, max_length=4)  # Field name made lowercase.
    titulo = models.CharField(db_column='titulo', unique=False, max_length=5)  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=50)  # Field name made lowercase.
    a_paterno = models.CharField(db_column='A_Paterno', max_length=50)  # Field name made lowercase.
    a_materno = models.CharField(db_column='A_Materno', max_length=50)  # Field name made lowercase.
    fecha_nac = models.DateField(db_column='Fecha_nac')  # Field name made lowercase.
    correo = models.CharField(db_column='Correo', max_length=100, blank=True)  # Field name made lowercase.
    celular = models.IntegerField(db_column='Celular', blank=True, null=True)  # Field name made lowercase.
    cuenta_bancaria = models.IntegerField(db_column='Cuenta_bancaria', blank=True, null=True)  # Field name made lowercase.
    nua = models.CharField(db_column='NUA', max_length=20, blank=True)  # Field name made lowercase.
    telefono = models.IntegerField(db_column='Telefono', blank=True, null=True)  # Field name made lowercase.
    direccion = models.CharField(db_column='direccion', unique=False, max_length=80, blank=True)  # Field name made lowercase.
    sexo = models.CharField(db_column='sexo', max_length=1, blank=True, null=True)  # Field name made lowercase.
    libreta_militar = models.CharField(db_column='libreta_militar', max_length=25, blank=True, null=True)  # Field name made lowercase.
    estado_civil = models.CharField(db_column='estado_civil', max_length=15, blank=True, null=True)  # Field name made lowercase.
    profesion = models.CharField(db_column='profesion', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ap_casada = models.CharField(db_column='ap_casada', max_length=25, blank=True, null=True)  # Field name made lowercase.
    nacionalidad = models.CharField(db_column='nacionalidad', max_length=50, blank=True, null=True)  # Field name made lowercase.
    foto1 = models.TextField(db_column='Foto1', blank=True, null=True)
    def cargo_actual(self):
        designacion=PrincipalDesignacion.objects.using('db_personal').filter(id_personal=self, estado='ACTIVO').last()
        if designacion:
            return designacion.id_cargo_id.nombre_cargo
        else:
             return 'Sin vinculo laboral'

    def __str__(self):
        return self.nombre + ' ' + self.a_paterno 

    def __unicode__(self):
        return self.nombre + ' ' + self.a_paterno 

    class Meta:
        managed = False
        db_table = 'principal_personal'

class PrincipalCargo(models.Model):
    id = models.AutoField(primary_key=True, db_column='id')  # Field name made lowercase.
    nombre_cargo = models.CharField(db_column='Nombre_cargo', max_length=80)  # Field name made lowercase.
    escala_salarial = models.IntegerField(db_column='Escala_salarial', blank=True, null=True)  # Field name made lowercase.
    estado = models.CharField(max_length=15)
    def __str__(self):        
        return self.nombre_cargo
    class Meta:
        managed = False
        db_table = 'principal_cargo'

class PrincipalDesignacion(models.Model):
    id = models.AutoField(primary_key=True)
    fecha_ingreso = models.DateField(db_column='Fecha_ingreso')  # Field name made lowercase.
    estado = models.CharField(db_column='Estado', max_length=15)  # Field name made lowercase.
    fecha_conclusion = models.DateField(db_column='Fecha_conclusion', blank=True, null=True)  # Field name made lowercase.
    id_cargo_id = models.ForeignKey(PrincipalCargo, db_column='id_cargo_id',
        on_delete=models.CASCADE)
      
    id_personal = models.ForeignKey('PrincipalPersonal', db_column='id_personal', blank=True, null=True,
        on_delete=models.CASCADE) #models.IntegerField(db_column='id_personal') #models.ForeignKey('PrincipalPersonal', db_column='id_personal', blank=True, null=True)
    item = models.IntegerField(db_column='Item')  # Field name made lowercase.
    tipo = models.CharField(db_column='tipo', max_length=25)  # Field name made lowercase.
    elaborado_por = models.CharField(db_column='elaborado_por', max_length=25, blank=True, null=True)  # Field name made lowercase.
    fecha_elaboracion = models.DateField(db_column='fecha_elaboracion', blank=True, null=True)  # Field name made lowercase.
    aprobado_por = models.CharField(db_column='aprobado_por', max_length=25, blank=True, null=True)  # Field name made lowercase.
    fecha_aprobacion = models.DateField(db_column='fecha_aprobacion', blank=True, null=True)  # Field name made lowercase.
    estado_registro = models.CharField(db_column='estado_registro', max_length=25)  # Field name made lowercase.
    solicitud = models.CharField(db_column='solicitud', max_length=25)  # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'principal_designacion'

class Reportes(models.Model):
    id = models.AutoField(primary_key=True)
    nombre_reporte = models.CharField(db_column='nombre_reporte', max_length=130)  # Field name made lowercase.
    ruta = models.CharField(db_column='ruta', max_length=250) 
    
class Parametrosreporte(models.Model):
    id = models.AutoField(primary_key=True)
    parametro = models.CharField(db_column='parametro', max_length=15)  # Field name made lowercase.
    reporte = models.ForeignKey(Reportes,  on_delete=models.CASCADE)

class Gestion(models.Model):
    id = models.AutoField(primary_key=True)
    gestion = models.IntegerField(db_column='gestion')  # Field name made lowercase.
    estado = models.CharField(db_column='estado', max_length=250) 

class Ampliacion(models.Model):
    id = models.AutoField(primary_key=True)
    asignacion = models.ForeignKey(Asignacion, db_column='asignacion_id',
        on_delete=models.CASCADE)
    tiempo = models.IntegerField()
    memorandum = models.IntegerField()
    tiempoauditor = models.IntegerField()    
    class Meta:
        managed = False

class Reasignacion(models.Model):
    id = models.AutoField(primary_key=True)
    asignacion = models.ForeignKey(Asignacion, db_column='asignacion_id',
        on_delete=models.CASCADE)
    nuevaasignacion = models.ForeignKey(Asignacion, db_column='nuevaasignacion_id',
        on_delete=models.CASCADE, related_name='nuevaasignacion_id')
    
    class Meta:
        managed = False

class ControlSupervisor(models.Model):
    id = models.AutoField(primary_key=True)
    asignacion = models.ForeignKey(Asignacion, db_column='asignacion_id',
        on_delete=models.CASCADE)
    asignacionsupervisor = models.ForeignKey(AsignacionSupervisor, db_column='asignacionsupervisor_id',
        on_delete=models.CASCADE)    
    etapa = models.ForeignKey(Etapa, db_column='etapa_id',
        on_delete=models.CASCADE)
    fecha = models.DateField()
    horas = models.IntegerField()
    class Meta:
        managed = False

class ControlDirector(models.Model):
    id = models.AutoField(primary_key=True)
    proyecto = models.ForeignKey(Proyecto, db_column='proyecto_id',
        on_delete=models.CASCADE)
    director = models.ForeignKey(Personal, db_column='director_id',
        on_delete=models.CASCADE)
    etapa = models.ForeignKey(Etapa, db_column='etapa_id',
        on_delete=models.CASCADE)
    fecha = models.DateField()
    horas = models.IntegerField()
    class Meta:
        managed = False