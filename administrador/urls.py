from django.contrib import admin
from django.urls import path
from . import views
from .views import *
from django.conf.urls import include
from . import views
from . import reportes
from django.conf.urls import url
from django.views.static import serve
from django.contrib.auth.decorators import login_required,permission_required
urlpatterns = [
    #url(r'^accounts/', include('registration.backends.simple.urls')),
    path('accounts/login',  views.login),        
    path('auditores', permission_required('administrador.view_personal')(views.PersonalListView.as_view()), name='lista_personal'),
    path('auditores/listar', permission_required('administrador.view_personal')(views.PersonalListView.as_view()), name='listar_auditor'),
    path('auditores/crear', permission_required('administrador.view_personal')(views.PersonalCreateView.as_view()), name='crear_auditor'),
    path('auditores/<pk>/update/', permission_required('administrador.view_personal')(PersonalUpdateView.as_view()), name='modificar_auditor'),
    path('auditores/<pk>/eliminar', permission_required('administrador.view_personal')(views.PersonalDeleteView.as_view()), name='eliminar_auditor'),
    path('auditores/<pk>/usuario', permission_required('administrador.view_personal')(views.UsuarioCreateView.as_view()), name='usuario_auditor'),

    path('proyectos', permission_required('administrador.view_personal')(views.ProyectoListView.as_view()), name='lista_proyecto'),
    path('proyectos/listar', permission_required('administrador.view_personal')(views.ProyectoListView.as_view()), name='listar_proyectos'),
    path('proyectos/crear', permission_required('administrador.view_personal')(views.ProyectoCreateView.as_view()), name='crear_proyecto'),
    path('proyectos/<pk>/update/', permission_required('administrador.view_personal')(ProyectoUpdateView.as_view()), name='modificar_proyecto'),
    path('proyectos/<pk>/eliminar', permission_required('administrador.view_personal')(views.ProyectoDeleteView.as_view()), name='eliminar_proyecto'),

    path('asignacion', permission_required('administrador.view_personal')(views.AsignacionListView.as_view()), name='lista_asignaciones'),
    path('asignacion/listar', permission_required('administrador.view_personal')(views.AsignacionListView.as_view()), name='listar_asignacion'),
    path('asignacion/crear', permission_required('administrador.view_personal')(views.AsignacionCreateView.as_view()), name='crear_asignacion'),
    path('asignacion/<pk>/update/', permission_required('administrador.view_personal')(AsignacionUpdateView.as_view()), name='modificar_asignacion'),
    path('asignacion/<pk>/eliminar', permission_required('administrador.view_personal')(views.AsignacionDeleteView.as_view()), name='eliminar_asignacion'),
    path('asignacion/memorandum/<pk>/', permission_required('administrador.view_personal')(views.memorandum_asignacion), name='memorandum_asignacion'),

    path('tipoasignacion', permission_required('administrador.view_personal')(views.TipoAsignacionListView.as_view()), name='lista_tipoasignacion'),
    path('tipoasignacion/listar', permission_required('administrador.view_personal')(views.TipoAsignacionListView.as_view()), name='lista_tipoasignacion'),
    path('tipoasignacion/crear', permission_required('administrador.view_personal')(views.TipoAsignacionCreateView.as_view()), name='crear_tipoasignacion'),
    path('tipoasignacion/<pk>/update/', permission_required('administrador.view_personal')(views.TipoAsignacionUpdateView.as_view()), name='modificar_tipoasignacion'),
    path('tipoasignacion/<pk>/eliminar', permission_required('administrador.view_personal')(views.TipoAsignacionDeleteView.as_view()), name='eliminar_tipoasignacion'),

    path('etapa', permission_required('administrador.view_personal')(views.EtapaListView.as_view()), name='listar_etapa'),
    path('etapa/listar', permission_required('administrador.view_personal')(views.EtapaListView.as_view()), name='listar_etapa'),
    path('etapa/crear', permission_required('administrador.view_personal')(views.EtapaCreateView.as_view()), name='crear_etapa'),
    path('etapa/<pk>/update/', permission_required('administrador.view_personal')(views.EtapaUpdateView.as_view()), name='modificar_etapa'),
    path('etapa/<pk>/eliminar', permission_required('administrador.view_personal')(views.EtapaDeleteView.as_view()), name='eliminar_etapa'),

    path('actividad', permission_required('administrador.view_personal')(views.ActividadListView.as_view()), name='lista_actividad'),
    path('actividad/listar', permission_required('administrador.view_personal')(views.ActividadListView.as_view()), name='listar_actividad'),
    path('actividad/crear', permission_required('administrador.view_personal')(views.ActividadCreateView.as_view()), name='crear_actividad'),
    path('actividad/<pk>/update/', permission_required('administrador.view_personal')(views.ActividadUpdateView.as_view()), name='modificar_actividad'),
    path('actividad/<pk>/eliminar', views.ActividadDeleteView.as_view(), name='eliminar_actividad'),

    path('estructura/<pk>', permission_required('administrador.view_personal')(views.EstructuraListView.as_view()), name='estructura'),
    path('estructura/crear', permission_required('administrador.view_personal')(views.EstructuraCreateView.as_view()), name='crear_estructura'),
    path('estructura/<pk>/update/', permission_required('administrador.view_personal')(views.EstructuraUpdateView.as_view()), name='modificar_estructura'),
    path('estructura/<pk>/eliminar', permission_required('administrador.view_personal')(views.EstructuraDeleteView.as_view()), name='eliminar_estructura'),
    path('buscarpersonal/', permission_required('administrador.view_personal')(views.buscar_personal), name='buscarpersonal'),  

    path('tiempodisponible/<pk>', permission_required('administrador.view_personal')(views.TiempoDisponibleListView.as_view()), name='tiempodisponible'),   

    path('tiempo_auditor/', permission_required('administrador.view_personal')(views.PoaPersonalListView.as_view()), name='tiempo_auditor'),
    path('modificar_poapersonal/<pk>', permission_required('administrador.view_personal')(views.PoaPersonalUpdateView.as_view()), name='modificar_poapersonal'),
    path('tipoauditor/', permission_required('administrador.view_personal')(views.TipoAuditorListView.as_view()), name='listar_tipoauditor'),
    path('tipoauditor/<pk>/update', permission_required('administrador.view_personal')(views.TipoAuditorUpdateView.as_view()), name='modificar_tipoauditor'),
    path('tipoauditor/<pk>/delete', permission_required('administrador.view_personal')(views.TipoAuditorDeleteView.as_view()), name='eliminar_tipoauditor'),
    path('tipoauditor/crear', permission_required('administrador.view_personal')(views.TipoAuditorCreateView.as_view()), name='crear_tipoauditor'),
    path('reportescostos', permission_required('administrador.view_personal')(views.ReportesListView.as_view()), name='listar_reportes'),
    path('<pk>/generarreporte', permission_required('administrador.view_personal')(views.GenReportesListView.as_view()), name='generar_reportes'),
    path('<pk>/ajustes_usuario', views.AjustesUsuarioView.as_view(), name='ajustes_usuario'),    

    path('asignacione', permission_required('administrador.view_personal')(views.AsignacionEListView.as_view()), name='lista_asignacionese'),
    path('asignacione/listar', permission_required('administrador.view_personal')(views.AsignacionListView.as_view()), name='listar_asignacione'),
    path('estructurae/<pk>/update/', permission_required('administrador.view_personal')(views.EstructuraEListView.as_view()), name='modificar_estructurae'),
    path('estructurae/crear', permission_required('administrador.view_personal')(views.EstructuraECreateView.as_view()), name='crear_estructurae'),
    path('estructurae/<pk>', permission_required('administrador.view_personal')(views.EstructuraEListView.as_view()), name='estructurae'),
    path('estructurae/<pk>/eliminar', permission_required('administrador.view_personal')(views.EstructuraEDeleteView.as_view()), name='eliminar_estructurae'),
    path('actualizar_orden/', permission_required('administrador.view_personal')(views.actualizar_orden), name='actualizar_orden'),
    path('modificarpassword/', views.cambiar_pass, name='modificarpassword'),
    path('listarsupervisores/', views.SupervisorListView.as_view(), name='listarsupervisores'),
    path('listarproysupervisor/<pk>/', views.ProyectoSupervisorListView.as_view(), name='listarproysupervisor'),
    path('gestiones/', views.GestionesListView.as_view(), name='gestiones'),
    path('gestiones/crear/', views.GestionCreateView.as_view(), name='creargestion'),
    path('cambiogestion/<pk>/', views.CambiarGestionUpdateView.as_view(), name='cambiogestion'),
    path('cambiar/<pk>/', views.cambiar, name='cambiar'),

    path('ampliacion/', views.AmpliacionesListView.as_view(), name='ampliacion'),
    path('ampliacion/crear/', permission_required('administrador.view_personal')(views.AmpliacionesCreateView.as_view()), name='crear_ampliacion'),
    path('ampliacion/buscar_trabajo', views.buscar_trabajo, name='ampliacion'), 

    path('asignacionmemo', permission_required('administrador.view_personal')(views.AsignacionMemoListView.as_view()), name='lista_asignacionesmemo'),
    path('asignacionmemo/listar', permission_required('administrador.view_personal')(views.AsignacionMemoListView.as_view()), name='listar_asignacionmemo'),
    path('asignacionmemo/crear/<pk>', permission_required('administrador.view_personal')(views.AsignacionMemoCreateView.as_view()), name='crear_asignacionmemo'),
    path('asignacionmemo/<pk>/update/', permission_required('administrador.view_personal')(AsignacionMemoUpdateView.as_view()), name='modificar_asignacionmemo'),
    path('asignacionmemo/<pk>/eliminar', permission_required('administrador.view_personal')(views.AsignacionMemoDeleteView.as_view()), name='eliminar_asignacionmemo'),    
    path('reporteresumenproyecto', (views.reporteresumenproyecto), name='reporteresumenproyecto'),
    path('listaproyectosresum', (views.listaproyectosresum), name='listaproyectosresum'),
    path('reportesgenerales', (views.reportesgenerales), name='reportesgenerales'),
    path('reportrelacioncargos', (views.reportrelacioncargos), name='reportrelacioncargos'),
    path('TiempoDisponibleUpdate/<pk>', permission_required('administrador.view_personal')(TiempoDisponibleUpdate.as_view()), name='modificar_tiempo_disponible'),    
    path('reportpoaaudi', permission_required('administrador.view_personal')(views.reportpoaaudi), name='reportpoaaudi'),    
]