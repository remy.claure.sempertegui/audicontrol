import json
import io
from django.conf import settings
from django.db import transaction
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from reportlab.platypus import Paragraph, Table, TableStyle
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter, landscape
from reportlab.lib.units import cm
from reportlab.lib import colors
from reportlab.lib.enums import TA_CENTER, TA_RIGHT

from django.http import HttpResponse, JsonResponse, Http404
from django.contrib.auth.mixins import LoginRequiredMixin

from audicontrol.mixins import ValidatePermissionRequiredMixin
from django.db.models.signals import pre_save
from django.shortcuts import  redirect
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import ListView, UpdateView, CreateView, DeleteView
from .models import *
from .forms import *
from django.views.decorators.csrf import requires_csrf_token, csrf_protect
from django.views.generic.edit import ModelFormMixin
from django.views.generic.edit import CreateView
from django.http import HttpResponseRedirect
import datetime as dt
from json import JSONEncoder
from .reportes import *
from django.shortcuts import render
from django.db.models import Q



# Create your views here.
@requires_csrf_token
@csrf_protect

def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


def login(request):
    username = request.POST.get('username', False)
    password = request.POST.get('password', False)
    user = auth.authenticate(username=username, password=password)

    if user is not None and user.is_active:
        # Correct password, and the user is marked "active"
        auth.login(request, user)
        # Redirect to a success page.
        return render(request,"registro/menu.html")
    else:
        # Show an error page
        return render(request,"registration/login.html")

def cambiar_pass(request):
    form = UsuariocpForm(data = request.POST)
    if request.method == 'POST':
       if form.is_valid:
          if request.POST['password1'] == request.POST['password']:
             u = User.objects.get(username=request.POST.get('username', False))
             u.set_password(request.POST.get('password1', False))
             u.save()
             return HttpResponseRedirect("/")
          else:
              error  = "Passwords should match (typo?)"
              return render(request,"registration/password_change_form.html", {
        'form': form})
    else:
         u = User.objects.get(username=request.user)
         form = UsuariocpForm(instance=u)
    return render(request,"registration/password_change_form.html", {
        'form': form, 'horas': horasdia(request)})

def logout(request):
    auth.logout(request)
    # Redirect to a success page.
    return render(request,"registration/login_error.html")

def login_error(request):
    auth.logout(request)
    # Redirect to a success page.
    return render(request,"registration/login_error.html")

class PersonalListView(ListView):
    model = Personal
    template_name = 'administrador/auditor.html'
    permission_required = 'administrador.view_personal'
    url_redirect = reverse_lazy('index')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                print('listo')
                data = []
                for personal in Personal.objects.all():
                    data.append(personal.toJSON())            
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)
    
    def get_success_url(self):
        return reverse('update_url', kwargs={
            'pk': self.object.pk,
        })

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'LISTADO DE PERSONAL'        
        context['action'] = 'searchdata'        
        context['create_url'] = reverse_lazy('crear_auditor')        
        context['list_url'] = reverse_lazy('listar_auditor')
        context['delete_url'] = reverse_lazy('eliminar_auditor')
        context['user_auditor'] = reverse_lazy('usuario_auditor')
        context['horas'] = horasdia(self.request)
        
        return context

class PersonalCreateView(CreateView):
    model = Personal
    form_class = PersonalForm
    template_name = 'administrador/crear_auditor.html'
    success_url = reverse_lazy('listar_auditor')
    permission_required = ''
    url_redirect = success_url    

    def get_form(self):
        form = super(PersonalCreateView, self).get_form()
        initial_base = self.get_initial() 
        return form

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                if form.is_valid():
                    form.save()
                    return HttpResponseRedirect(self.url_redirect)
                else:
                    data['error'] = form.errors
            else:
                data['error'] = 'No ha ingresado a ninguna opcion'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)
    def get_context_data(self, **kwargs):
        context = super(PersonalCreateView, self).get_context_data(**kwargs)
        context['title'] = 'Añadir personal'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class PersonalUpdateView(UpdateView):
    model = Personal
    form_class = PersonalForm
    template_name = 'administrador/crear_auditor.html'
    success_url = reverse_lazy('listar_auditor')
    permission_required = ''
    url_redirect = success_url  

    def get_context_data(self, **kwargs):
        context = super(PersonalUpdateView, self).get_context_data(**kwargs)
        context['title'] = 'Añadir personal'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class PersonalDeleteView(DeleteView):
    model = Personal
    form_class = PersonalForm
    template_name = 'administrador/crear_auditor.html'
    success_url = reverse_lazy('listar_auditor')
    permission_required = ''
    url_redirect = success_url    

    def get_context_data(self, **kwargs):
        context = super(PersonalDeleteView, self).get_context_data(**kwargs)
        context['title'] = 'Eliminar personal'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class ProyectoListView(ListView):
    model = Proyecto
    template_name = 'administrador/proyecto.html'
    permission_required = ''
    url_redirect = reverse_lazy('index')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super(ProyectoListView, self).get_queryset().filter(fecha_inicio__year=self.request.session['gestion'])

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for proyecto in Proyecto.objects.all():
                    data.append(proyecto.toJSON())            
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        proyectos = Proyecto.objects.all()
        totalplazos = 0
        totalplazosasignado = 0
        for proyecto in proyectos:
            totalplazos = totalplazos + proyecto.plazo
            totalplazosasignado = totalplazosasignado + proyecto.plazo_asignado()
        context = super().get_context_data(**kwargs)
        context['title'] = 'LISTADO DE PROYECTOS'        
        context['action'] = 'searchdata'        
        context['create_url'] = reverse_lazy('crear_proyecto')
        context['list_url'] = reverse_lazy('lista_proyecto')
        context['totalplazo'] = totalplazos
        context['totalplazoasignado'] = totalplazosasignado
        context['horas'] = horasdia(self.request)

        return context

class ProyectoCreateView(CreateView):
    model = Proyecto
    form_class = ProyectoForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('listar_proyectos')
    permission_required = ''
    url_redirect = success_url    

    def get_form(self):
        form = super(ProyectoCreateView, self).get_form()
        initial_base = self.get_initial() 
        return form

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                if form.is_valid():
                    form.save()
                    return HttpResponseRedirect(self.url_redirect)
                else:
                    data['error'] = form.errors
            else:
                data['error'] = 'No ha ingresado a ninguna opcion'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super(ProyectoCreateView, self).get_context_data(**kwargs)
        context['title'] = 'Añadir trabajo'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class ProyectoUpdateView(UpdateView):
    model = Proyecto
    form_class = ProyectoForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('listar_proyectos')
    permission_required = ''
    url_redirect = success_url  

    def get_context_data(self, **kwargs):
        context = super(ProyectoUpdateView, self).get_context_data(**kwargs)
        context['title'] = 'Modificar Trabajo'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class ProyectoDeleteView(DeleteView):
    model = Proyecto
    form_class = ProyectoForm
    template_name = 'administrador/crear_auditor.html'
    success_url = reverse_lazy('listar_proyectos')
    permission_required = ''
    url_redirect = success_url    

    def get_context_data(self, **kwargs):
        context = super(ProyectoDeleteView, self).get_context_data(**kwargs)
        context['title'] = 'Eliminar personal'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class AsignacionListView(ListView):
    model = Asignacion
    template_name = 'administrador/asignacion.html'
    permission_required = ''
    url_redirect = reverse_lazy('index')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super(AsignacionListView, self).get_queryset().filter(poa=True, gestion=self.request.session['gestion'])

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for asignacion in Asignacion.objects.filter(poa=True):
                    data.append(asignacion.toJSON())            
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'ASIGNACION DE PROYECTOS'        
        context['action'] = 'searchdata'        
        context['create_url'] = reverse_lazy('crear_asignacion')
        context['list_url'] = reverse_lazy('lista_asignaciones')
        context['horas'] = horasdia(self.request)
        return context

class AsignacionCreateView(CreateView):
    model = Asignacion
    form_class = AsignacionForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('listar_asignacion')
    permission_required = ''
    url_redirect = success_url    

    def get_form(self):
        form = super(AsignacionCreateView, self).get_form()
        initial_base = self.get_initial() 
        return form

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                if form.is_valid():
                    form.save()
                    return HttpResponseRedirect(self.url_redirect)
                else:
                    data['error'] = form.errors
            else:
                data['error'] = 'No ha ingresado a ninguna opcion'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super(AsignacionCreateView, self).get_context_data(**kwargs)
        context['title'] = 'Añadir Asignacion'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class AsignacionUpdateView(UpdateView):
    model = Asignacion
    form_class = AsignacionForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('listar_asignacion')
    permission_required = ''
    url_redirect = success_url  

    def get_context_data(self, **kwargs):
        context = super(AsignacionUpdateView, self).get_context_data(**kwargs)
        context['title'] = 'Asignar proyecto a personal'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class AsignacionDeleteView(DeleteView):
    model = Asignacion
    form_class = ProyectoForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('listar_asignacion')
    permission_required = ''
    url_redirect = success_url    

    def get_context_data(self, **kwargs):
        context = super(AsignacionDeleteView, self).get_context_data(**kwargs)
        context['title'] = 'Eliminar asignacion'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context



class TipoAsignacionListView(ListView):
    model = TipoAsignacion
    template_name = 'administrador/tipoasignacion.html'
    permission_required = ''
    url_redirect = reverse_lazy('index')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for tipoasignacion in TipoAsignacion.objects.all():
                    data.append(tipoasignacion.toJSON())            
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'TIPOS DE ASIGNACIONES'        
        context['action'] = 'searchdata'        
        context['create_url'] = reverse_lazy('crear_tipoasignacion')
        context['list_url'] = reverse_lazy('lista_tipoasignacion')
        context['horas'] = horasdia(self.request)
        return context

class TipoAsignacionCreateView(CreateView):
    model = TipoAsignacion
    form_class = TipoAsignacionForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('listar_asignacion')
    permission_required = ''
    url_redirect = success_url    

    def get_form(self):
        form = super(TipoAsignacionCreateView, self).get_form()
        initial_base = self.get_initial() 
        return form

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                if form.is_valid():
                    form.save()
                    return HttpResponseRedirect(self.url_redirect)
                else:
                    data['error'] = form.errors
            else:
                data['error'] = 'No ha ingresado a ninguna opcion'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super(TipoAsignacionCreateView, self).get_context_data(**kwargs)
        context['title'] = 'Añadir Tipo de Asignacion'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class TipoAsignacionUpdateView(UpdateView):
    model = TipoAsignacion
    form_class = TipoAsignacionForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('lista_tipoasignacion')
    permission_required = ''
    url_redirect = success_url  

    def get_context_data(self, **kwargs):
        context = super(TipoAsignacionUpdateView, self).get_context_data(**kwargs)
        context['title'] = 'Asignar proyecto a personal'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        return context

class TipoAsignacionDeleteView(DeleteView):
    model = TipoAsignacion
    form_class = TipoAsignacionForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('lista_tipoasignacion')
    permission_required = ''
    url_redirect = success_url    

    def get_context_data(self, **kwargs):
        context = super(TipoAsignacionDeleteView, self).get_context_data(**kwargs)
        context['title'] = 'Eliminar Tipo Asignacion'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context


class EtapaListView(ListView):
    model = Etapa
    template_name = 'administrador/etapa.html'
    permission_required = ''
    url_redirect = reverse_lazy('index')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for etapa in Etapa.objects.all():
                    data.append(etapa.toJSON())            
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'ETAPAS'        
        context['action'] = 'searchdata'        
        context['create_url'] = reverse_lazy('crear_etapa')
        context['list_url'] = reverse_lazy('listar_etapa')
        context['horas'] = horasdia(self.request)
        return context

class EtapaCreateView(CreateView):
    model = Etapa
    form_class = EtapaForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('listar_etapa')
    permission_required = ''
    url_redirect = success_url    

    def get_form(self):
        form = super(EtapaCreateView, self).get_form()
        initial_base = self.get_initial() 
        return form

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                if form.is_valid():
                    form.save()
                    return HttpResponseRedirect(self.url_redirect)
                else:
                    data['error'] = form.errors
            else:
                data['error'] = 'No ha ingresado a ninguna opcion'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super(EtapaCreateView, self).get_context_data(**kwargs)
        context['title'] = 'Añadir Etapa'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class EtapaUpdateView(UpdateView):
    model = Etapa
    form_class = EtapaForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('listar_etapa')
    permission_required = ''
    url_redirect = success_url  

    def get_context_data(self, **kwargs):
        context = super(EtapaUpdateView, self).get_context_data(**kwargs)
        context['title'] = 'Modificar Etapa'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class EtapaDeleteView(DeleteView):
    model = Etapa
    form_class = EtapaForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('listar_etapa')
    permission_required = ''
    url_redirect = success_url    

    def get_context_data(self, **kwargs):
        context = super(EtapaDeleteView, self).get_context_data(**kwargs)
        context['title'] = 'Eliminar Etapa'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class ActividadListView(ListView):
    model = Actividad
    template_name = 'administrador/actividad.html'
    permission_required = ''
    url_redirect = reverse_lazy('index')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for actividad in Actividad.objects.all():
                    data.append(etapa.toJSON())            
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'ACTIVIDADES'        
        context['action'] = 'searchdata'        
        context['create_url'] = reverse_lazy('crear_actividad')
        context['list_url'] = reverse_lazy('lista_actividad')
        context['horas'] = horasdia(self.request)
        return context

class ActividadCreateView(CreateView):
    model = Actividad
    form_class = ActividadForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('listar_actividad')
    permission_required = ''
    url_redirect = success_url    

    def get_form(self):
        form = super(ActividadCreateView, self).get_form()
        initial_base = self.get_initial() 
        return form

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                if form.is_valid():
                    form.save()
                    return HttpResponseRedirect(self.url_redirect)
                else:
                    data['error'] = form.errors
            else:
                data['error'] = 'No ha ingresado a ninguna opcion'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super(ActividadCreateView, self).get_context_data(**kwargs)
        context['title'] = 'Añadir Actividad'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class ActividadUpdateView(UpdateView):
    model = Actividad
    form_class = ActividadForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('listar_actividad')
    permission_required = ''
    url_redirect = success_url  

    def get_context_data(self, **kwargs):
        context = super(ActividadUpdateView, self).get_context_data(**kwargs)
        context['title'] = 'Modificar Actividad'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class ActividadDeleteView(DeleteView):
    model = Actividad
    form_class = ActividadForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('listar_actividad')
    permission_required = ''
    url_redirect = success_url    

    def get_context_data(self, **kwargs):
        context = super(ActividadDeleteView, self).get_context_data(**kwargs)
        context['title'] = 'Eliminar Actividad'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class EstructuraListView(ListView):
    model = TEA
    template_name = 'administrador/estructura.html'
    permission_required = ''
    url_redirect = reverse_lazy('estructura')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def get_queryset(self):
        return super(EstructuraListView, self).get_queryset().filter(asignacion=self.kwargs['pk'])

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
               form=TEAForm(request.POST)
               if form.is_valid():
                  form.save()
                  return HttpResponseRedirect(self.url_redirect)
               else:
                    data['error1'] = form.errors
            else:
                data['error1'] = 'No ha ingresado a ninguna opcion'
        except Exception as e:
            data['error1'] = str(e)
        return redirect(reverse_lazy('estructura', kwargs={'pk': request.POST['asignacion'] })) 

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'TIPO DE ASIGNACION'        
        context['action'] = 'searchdata'  
        context['etapas'] = Etapa.objects.all()      
        asignacion=Asignacion.objects.filter(id=self.kwargs['pk']).first()
        context['proyecto'] = asignacion.proyecto.nombre_proyecto
        context['responsable'] = asignacion.personal.nombre + ' ' + asignacion.personal.a_paterno + ' ' + asignacion.personal.a_materno
        context['actividades'] = Actividad.objects.all().order_by('nombre_actividad')      
        context['create_url'] = reverse_lazy('crear_estructura')
        context['update_url'] = reverse_lazy('modificar_estructura')
        context['delete_url'] = reverse_lazy('eliminar_estructura')
        context['id'] = self.kwargs['pk']
        context['horas'] = horasdia(self.request)
        return context


class EstructuraCreateView(CreateView):
    model = TEA
    form_class = TEAForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('modificar_estructurae')
    permission_required = ''
    url_redirect = success_url  
   
    def get_form(self):
        form = super(EstructuraCreateView, self).get_form()
        initial_base = self.get_initial() 
        return form

    def post(self, request, *args, **kwargs):
        data = {}
        action = request.POST['action']
        print(action)
        if action == 'add':
           form = self.get_form()
           if form.is_valid():
              form.save()
              return HttpResponseRedirect(self.url_redirect)
           else:
                data['error1'] = form.errors
        else:
                data['error1'] = 'No ha ingresado a ninguna opcion'
 
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super(EstructuraCreateView, self).get_context_data(**kwargs)
        context['title'] = 'Añadir Actividad'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        
        return context

class EstructuraUpdateView(UpdateView):
    model = TEA
    form_class = TEAForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('listar_actividad')
    permission_required = ''
    url_redirect = success_url  

    def get_context_data(self, **kwargs):
        context = super(EstructuraUpdateView, self).get_context_data(**kwargs)
        context['title'] = 'Modificar Actividad'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class EstructuraDeleteView(DeleteView):
    model = TEA
    form_class = TEAForm
    template_name = 'administrador/crear_proyecto.html'    
    success_url = reverse_lazy('estructura', args={'1'})
    permission_required = ''
    url_redirect = success_url    

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs) 

    def get_context_data(self, **kwargs):
        context = super(EstructuraDeleteView, self).get_context_data(**kwargs)
        context['title'] = 'Eliminar Actividad de la estructura'
        reverse_lazy('estructura', args={self.kwargs['pk']})
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

def memorandum_asignacion(request, pk):
    asignacion = Asignacion.objects.filter(pk=pk).first()#get_object_or_404(Asignacion, pk=request.GET.get('id'))
    con = 0

    def headPdf(c):
        from datetime import datetime
        image = '/static/images/logo.jpg'
        image2 = '/static/images/escudo.jpg'
        # images
        #c.drawImage(image, 340, 730, width=30, height=30)
        c.drawImage(image2, 10, 730, width=40, height=35)
        c.setLineWidth(.3)
        c.setFont('Helvetica-Oblique', 8)
        c.drawString(50, 750, 'GOBIERNO AUTÓNOMO DEPARTAMENTAL DE POTOSÍ')
        c.drawString(55, 740, 'DIRECCION DEPARTAMENTAL DE AUDITORIA INTERNA')

        c.setFont('Helvetica-Bold', 12)
        c.drawString(502, 750, "N°")        
        c.drawString(520, 750, '0'+str(asignacion.nro_memorandum)+'/'+str(asignacion.gestion_memorandum))
        c.setFont('Helvetica-Bold', 14)
        c.drawString(252, 710, "MEMORANDUM")
        c.setFont('Helvetica', 10)
        months = ("Enero", "Febrero", "Marzo", "Abri", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
        fecha=asignacion.proyecto.fecha_inicio
        day = fecha.day
        month = months[fecha.month - 1]
        year = fecha.year
        message = "{} de {} del {}".format(day, month, year)
        c.drawString(30, 630, 'Potosí, '+message)
        if asignacion.personal.sexo=='M':
            titulo1 = 'Señor:'
        else: 
            titulo1 = 'Señora:'
        c.drawString(320, 680, titulo1)
        c.drawString(320, 665, asignacion.personal.profesion.titulo + asignacion.personal.nombre + ' ' + asignacion.personal.a_paterno + ' ' + asignacion.personal.a_materno)
        c.setFont('Helvetica-Bold', 8)
        c.drawString(320, 650, asignacion.cargo)
        c.setFont('Helvetica', 8)
        c.drawString(320, 630, 'PRESENTE.-')
        c.line(10, 607, 580, 607)
        c.line(10, 609, 580, 609)
        c.line(300, 700, 300, 609)
        c.setFont('Helvetica', 9)
        c.drawString(30, 580, titulo1+':')
        if asignacion.supervisor.profesion.titulo:
           titulo1=asignacion.supervisor.profesion.titulo
        else:
             titulo1 = ''
        supervisor=titulo1 + asignacion.supervisor.nombre + ' ' + asignacion.supervisor.a_paterno + ' ' + asignacion.supervisor.a_materno
        texto = 'Con el objectivo de cumplir con el POA de la gestion '+str(asignacion.gestion_memorandum)+' para la Dirección de Auditoria Interna, dentro del tiempo de las auditorias programadas, por instrucciones de mi autoridad usted deberá efectuar las siguientes auditorias:'
        texto1 = texto[:130]
        texto2 = texto[130:]
        texto3 = texto[160:80]

        c.drawString(30, 565, texto1)
        c.drawString(30, 550, texto2)
        c.drawString(30, 535, texto3)

        c.setFont('Helvetica-Bold', 9)  
        texto = asignacion.tipoasignacion.nombre_tipo+' '+asignacion.proyecto.nombre_proyecto
        texto1 = texto[:120]
        texto2 = texto[120:]
        c.drawString(30, 515, texto1)
        c.drawString(30, 500, texto2)

        c.setFont('Helvetica', 9)  
        texto = 'El trabajo encomendado debe ejecutar desde la fecha del presente memorándum en un plazo de '+str(asignacion.plazo)+' días hábiles, debiendo emitir informe y legajos de papeles de trabajo a su conclusión, considerando las Normas de Auditoría gubernamental emitidas por la Contraloria General del Estado, y bajo la supervisión del Lic. ' + supervisor
        texto1 = texto[:126]
        texto2 = texto[126:260]
        texto3 = texto[261:400]
        c.drawString(30, 475, texto1)
        c.drawString(30, 460, texto2)
        c.drawString(30, 445, texto3)
        c.drawString(30, 430, 'Con este motivo, saludo a usted atentamente.')
        
        # start 170, height end y, height
       

    def datos_generales(c):
        c.line(30, 525, 750, 525)
        c.setFont('Helvetica', 9)
        

    def style_table_size(c):
        width, height = letter
        table = Table(data, colWidths=[1.3 * cm, 12.7 * cm, 2.2 * cm, 3 * cm, 3 * cm, 3.5 * cm])
        table.setStyle(TableStyle([
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ]))

        table.wrapOn(c, width, height)
        table.drawOn(c, 30, high)

    # Create the HttpResponse headers with PDF
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=Daler.pdf'

    # Create pdf object
    buffer = io.BytesIO()
    c = canvas.Canvas(buffer, pagesize=(letter))
    headPdf(c)
    # Table header styles
    styles = getSampleStyleSheet()
    styleBH = styles["Normal"]
    styleBH.alignment = TA_CENTER
    styleBH.fontSize = 9
    # table content
    styles = getSampleStyleSheet()
    styleBA = styles["Normal"]
    styleBA.alignment = TA_RIGHT
    styleBA.fontSize = 8

    styles = getSampleStyleSheet()
    styleBB = styles["Normal"]
    styleBB.alignment = TA_CENTER
    styleBB.fontSize = 8   

    # Table
    high = 470
    data = []
    temporal = []
   

    # start 170, height end y, height
   

    c.save()
    # get the value of BytesIO buffer and write response
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)
    return response

class UsuarioCreateView(CreateView):
    model = User
    form_class = UsuarioForm
    template_name = 'administrador/crearusuario.html'
    success_url = reverse_lazy('listar_auditor')
    permission_required = ''
    url_redirect = success_url    
    auditor_id=0
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def get_form(self):
        form = super(UsuarioCreateView, self).get_form()
        initial_base = self.get_initial() 
        if self.kwargs['pk']:
           auditor_id = self.kwargs['pk']
        else:    
           auditor_id = 0
        self.auditor_id = auditor_id
        print(auditor_id)
        return form

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            
            if action == 'add':
                form = self.get_form()
                if form.is_valid():
                   formulario = form.save(commit=False)
                   # si no estableces el estado activo en el form de arriba tambien puedes ponerlo aqui o modificar otro campo que tu quieras
                   # formulario.estado = 'activo'
                   formulario.set_password(formulario.password)
                   auditor_id = request.POST.get('auditor_id', '')
                   if auditor_id:
                       auditor= Personal.objects.get(id=auditor_id)
                   else:
                       auditor = None 
                   print(auditor)
                   formulario.save()
                   usuario = User.objects.all().last()
                   print(usuario.id)
                   usuario_id = usuario.id
                   auditor.usuario_id=usuario_id
                   auditor.save()
                   #formulario.save()
                   
                   return HttpResponseRedirect(self.url_redirect)
                else:
                    data['error'] = form.errors
            else:
                data['error'] = 'No ha ingresado a ninguna opcion'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)
    def get_context_data(self, **kwargs):
        context = super(UsuarioCreateView, self).get_context_data(**kwargs)
        context['title'] = 'Crear usuario para el Auditor'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['auditor_id'] = self.auditor_id
        context['horas'] = horasdia(self.request)
        return context

def defaultencode(o):
    if type(o) is dt.date or type(o) is dt.datetime or type(o) is dt.time:
        return o.isoformat()

class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj, 'isoformat'): #handles both date and datetime objects
            return obj.isoformat()
        else:
            return json.JSONEncoder.default(self, obj)

def buscar_personal(request):
    ci = request.GET.get('ci','')
    ur = PrincipalPersonal.objects.using('db_personal').filter(ci=ci).first()
    if ur:
       print('encontrado')
       return HttpResponse(  json.dumps(
                        [
                            {
                                'id': ur.id,
                                'nombre': ur.nombre, 
                                'a_paterno': ur.a_paterno, 
                                'a_materno': ur.a_materno, 
                                'cargo': ur.cargo_actual(), 
                                'profesion': ur.profesion, 
                                'sexo': ur.sexo, 
                            },
                        ]
                ), content_type = 'application/json')
    else:
        print('no hay')
        return HttpResponse(json.dumps(
                        [
                            {
                                'id': 'XXXXXXXX',
                                'nombre': 'XXXXXXXX', 
                                'a_paterno': 'XXXXXXXX', 
                                'a_materno': 'XXXXXXXX', 
                                'cargo': 'XXXXXXXX', 
                                'sexo': 'XXXXXXXX',
                            },
                        ]
                ), content_type = 'application/json')
              
   
class TiempoDisponibleListView(ListView):
    model = TiempoDisponible
    template_name = 'administrador/tiempodisponible.html'
    permission_required = ''
    url_redirect = reverse_lazy('tiempodisponible')
    pk = 2021
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def get_queryset(self):
        self.pk=self.request.session['gestion']
        return super(TiempoDisponibleListView, self).get_queryset().filter(gestion=self.pk)    
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tiemposgestion = TiempoDisponible.objects.filter(gestion=self.pk)
        tsabados_domingos = 0
        tferiados = 0
        tdias_totales = 0
        tdias_habiles = 0
        for tiempogestion in tiemposgestion:
            tsabados_domingos=tiempogestion.sabados_domingos + tsabados_domingos
            tferiados=tiempogestion.feriados + tferiados
            tdias_totales=tiempogestion.dias_totales + tdias_totales
            tdias_habiles=tiempogestion.dias_habiles + tdias_habiles

        context['title'] = 'TIEMPO DISPONIBLE POR GESTION'        
        context['action'] = 'searchdata'  
        context['gestion'] = self.kwargs['pk']        
        context['tsabados_domingos'] = tsabados_domingos
        context['tferiados'] = tferiados
        context['tdias_totales'] = tdias_totales
        context['tdias_habiles'] = tdias_habiles
        context['horas'] = horasdia(self.request)
        return context    

class PoaPersonalListView(ListView):
    model = PoaPersonal
    template_name = 'administrador/poapersonal.html'
    permission_required = ''
    url_redirect = reverse_lazy('index')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super(PoaPersonalListView, self).get_queryset().filter(gestion=self.request.session['gestion'])
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                print('listo')
                data = []
                for poapersonal in PoaPersonal.objects.filter(gestion=self.request.session['gestion']):
                    data.append(poapersonal.toJSON())            
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)
    
    def get_success_url(self):
        return reverse('update_url', kwargs={
            'pk': self.object.pk,
        })

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'TIEMPO EFECTIVO INDIVIDUAL'        
        context['action'] = 'searchdata'        
        context['create_url'] = reverse_lazy('crear_auditor')        
        context['list_url'] = reverse_lazy('listar_auditor')
        context['delete_url'] = reverse_lazy('eliminar_auditor')
        context['user_auditor'] = reverse_lazy('usuario_auditor')
        context['horas'] = horasdia(self.request)
        poaspersonal = PoaPersonal.objects.filter(gestion=self.request.session['gestion'])
        tdias_anio = 0
        tsabados_domingos = 0
        tferiados = 0
        tdias_habiles = 0
        tdias_vacacion = 0
        tdias_lactancia = 0
        tdias_capacitacion = 0
        tdias_laborales = 0
        tdias_administrativos = 0
        tdias_efectivos = 0
        tdias_no_prog = 0  
        tdias_prog = 0

        for poapersonal in poaspersonal:
            tdias_anio = tdias_anio + 365
            tsabados_domingos = tsabados_domingos + poapersonal.sabados_domingos
            tferiados = tferiados + poapersonal.dias_feriados
            tdias_habiles = tdias_habiles + poapersonal.dias_habiles
            tdias_vacacion = tdias_vacacion + poapersonal.dias_vacacion
            tdias_lactancia = tdias_lactancia + poapersonal.dias_lactancia
            tdias_capacitacion = tdias_capacitacion + poapersonal.dias_capacitacion
            tdias_laborales = tdias_laborales + poapersonal.dias_laborales()
            tdias_administrativos = tdias_administrativos + poapersonal.dias_administrativos
            tdias_efectivos = tdias_efectivos + poapersonal.dias_efectivos()
            tdias_no_prog = tdias_no_prog + poapersonal.dias_no_prog
            tdias_prog = tdias_prog + poapersonal.dias_prog()

        context['tdias_anio'] = tdias_anio 
        context['tsabados_domingos'] = tsabados_domingos
        context['tferiados'] = tferiados 
        context['tdias_habiles'] = tdias_habiles 
        context['tdias_vacacion'] = tdias_vacacion 
        context['tdias_lactancia'] = tdias_lactancia 
        context['tdias_capacitacion'] = tdias_capacitacion 
        context['tdias_laborales'] = tdias_laborales 
        context['tdias_administrativos'] = tdias_administrativos 
        context['tdias_efectivos'] = tdias_efectivos 
        context['tdias_no_prog'] = tdias_no_prog 
        context['tdias_prog'] = tdias_prog 
        
        return context

class PoaPersonalUpdateView(UpdateView):
    model = PoaPersonal
    form_class = PoaPersonalForm
    template_name = 'administrador/crear_poapersonal.html'
    success_url = reverse_lazy('tiempo_auditor')
    permission_required = ''
    url_redirect = success_url      

    def get_context_data(self, **kwargs):
        context = super(PoaPersonalUpdateView, self).get_context_data(**kwargs)
        context['title'] = 'Modificar POA Individual'
        context['update_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class TipoAuditorListView(ListView):
    model = TipoAuditor
    template_name = 'administrador/tipoauditor.html'
    permission_required = ''
    url_redirect = reverse_lazy('create_tipoauditor')
    success_url = reverse_lazy('crear_tipoauditor')
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def get_queryset(self):

        return super(TipoAuditorListView, self).get_queryset().all()

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
               form=TipoAuditorForm(request.POST)
               if form.is_valid():
                  form.save()
                  return HttpResponseRedirect(self.url_redirect)
               else:
                    data['error1'] = form.errors
            else:
                data['error1'] = 'No ha ingresado a ninguna opcion'
        except Exception as e:
            data['error1'] = str(e)
        return redirect(reverse_lazy('list_tipoauditor')) 
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'TIPOS DE AUDITORES'        
        context['action'] = 'searchdata'  
        context['create_url'] = self.success_url
        context['horas'] = horasdia(self.request)
        return context 

class TipoAuditorUpdateView(UpdateView):
    model = TipoAuditor
    form_class = TipoAuditorForm
    template_name = 'administrador/crear_tipoauditor.html'
    success_url = reverse_lazy('listar_tipoauditor')
    permission_required = ''
    url_redirect = success_url      

    def get_context_data(self, **kwargs):
        context = super(TipoAuditorUpdateView, self).get_context_data(**kwargs)
        context['title'] = 'Modificar Tipo Auditor'
        context['update_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class TipoAuditorDeleteView(DeleteView):
    model = TipoAuditor
    form_class = TipoAuditorForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('listar_tipoauditor')
    permission_required = ''
    url_redirect = success_url    

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs) 

    def get_context_data(self, **kwargs):
        context = super(TipoAuditorDeleteView, self).get_context_data(**kwargs)
        context['title'] = 'Eliminar Tipo de Auditor'
        context['list_url'] = self.success_url        
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class TipoAuditorCreateView(CreateView):
    model = TipoAuditor
    form_class = TipoAuditorForm
    template_name = 'administrador/crear_tipoauditor.html'
    success_url = reverse_lazy('listar_tipoauditor')
    permission_required = ''
    url_redirect = success_url  
   
    def get_form(self):
        form = super(TipoAuditorCreateView, self).get_form()
        initial_base = self.get_initial() 
        return form

    def post(self, request, *args, **kwargs):
        data = {}
        action = request.POST['action']
        print(action)
        if action == 'add':
           form = self.get_form()
           if form.is_valid():
              form.save()
              return HttpResponseRedirect(self.url_redirect)
           else:
                data['error1'] = form.errors
        else:
                data['error1'] = 'No ha ingresado a ninguna opcion'
 
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super(TipoAuditorCreateView, self).get_context_data(**kwargs)
        context['title'] = 'Añadir Tipo de Auditor'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        
        return context

class ReportesListView(ListView):
    model = Reportes
    template_name = 'administrador/reportes_list.html'
    permission_required = ''
    url_redirect = reverse_lazy('listar_reportes')
    success_url = reverse_lazy('listar_reportes')
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def get_queryset(self):

        return super(ReportesListView, self).get_queryset().all()

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
               form=ReportesForm(request.POST)
               if form.is_valid():
                  form.save()
                  return HttpResponseRedirect(self.url_redirect)
               else:
                    data['error1'] = form.errors
            else:
                data['error1'] = 'No ha ingresado a ninguna opcion'
        except Exception as e:
            data['error1'] = str(e)
        return redirect(reverse_lazy('listar_reportes')) 
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'TIPOS DE AUDITORES'        
        context['action'] = 'searchdata'  
        context['create_url'] = self.success_url
        context['generar_url'] = self.success_url
        context['horas'] = horasdia(self.request)
        return context 

class GenReportesListView(ListView):
    model = Parametrosreporte
    template_name = 'administrador/generarreportes.html'
    permission_required = ''
    pk = 1
    url_redirect = reverse_lazy('listar_reportes')
    success_url = reverse_lazy('listar_reportes')
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.pk=self.kwargs['pk']
        return super().dispatch(request, *args, **kwargs) 
    
    
    def get_queryset(self):
        return super(GenReportesListView, self).get_queryset().filter(reporte_id=self.kwargs['pk'])

    def post(self, request, *args, **kwargs):
        data = {}        
        reportecostos=Reportes.objects.get(id=self.pk)
        reportecostos2=Reportes.objects.all()
        xparametro1 = request.POST.get('parametro1','') 
        xparametro2 = request.POST.get('parametro2','') 
        xparametro3 = request.POST.get('parametro3','') 
        xparametro4 = request.POST.get('parametro4','') 
        
        #report = ReporteCostos(reportecostos2, reportecostos.ruta , xparametro)     
        report = ReporteCostos(reportecostos2, reportecostos.ruta , xparametro1, xparametro1, xparametro2, xparametro3, xparametro4)                  
        return report.render_to_response()
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'TIPOS DE AUDITORES'        
        context['action'] = 'add'  
        context['create_url'] = self.success_url
        context['reporteid'] = self.kwargs['pk']
        context['horas'] = horasdia(self.request)
        return context 

class AjustesUsuarioView(UpdateView):
    model = Personal
    form_class = PersonalForm2
    template_name = 'administrador/crear_auditor.html'
    success_url = '/'
    permission_required = ''
    url_redirect = success_url   
    
    def post(self, request, *args, **kwargs):
        data = {}
        action = request.POST['action']
        if action == 'add':
           personal = Personal.objects.filter(ci=request.POST.get('ci')).first()  
           reportes = Reportes.objects.all()
           form = PersonalForm2(request.POST, instance=personal)
           if form.is_valid():              
              personal=form.save(commit=False)
              personal.save()
              report = ReporteActualizacionAuditor(reportes, personal.id)
              return report.render_to_response()
           else:
                data['error1'] = form.errors
        else:
                data['error1'] = 'No ha ingresado a ninguna opcion'
        
        return JsonResponse(data)

    def get_queryset(self):
        personal = Personal.objects.filter(usuario=self.request.user).first()
        self.kwargs['pk']=personal.id
        return super(AjustesUsuarioView, self).get_queryset().all()

    def get_context_data(self, **kwargs):
        context = super(AjustesUsuarioView, self).get_context_data(**kwargs)
        context['title'] = 'Modificar perfil del auditor'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context


class AsignacionEListView(ListView):
    model = Asignacion
    template_name = 'administrador/asignacione.html'
    permission_required = ''
    url_redirect = reverse_lazy('index')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for asignacion in Asignacion.objects.all():
                    data.append(asignacion.toJSON())            
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'ASIGNACION DE PROYECTOS'        
        context['action'] = 'searchdata'        
        context['create_url'] = reverse_lazy('crear_asignacion')
        context['list_url'] = reverse_lazy('lista_asignaciones')
        context['horas'] = horasdia(self.request)
        return context

class EstructuraEListView(ListView):
    model = TEA
    template_name = 'administrador/estructurae.html'
    permission_required = ''
    url_redirect = reverse_lazy('estructurae')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def get_queryset(self):
        return super(EstructuraEListView, self).get_queryset().filter(asignacion=self.kwargs['pk']).order_by('orden')

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
               form=TEAForm(request.POST)
               if form.is_valid():
                  tea=form.save(commit=false)
                  tea1 = TEA.objects.filter(asignacion=tea.asignacion, etapa=tea.etapa).order_by('-orden').first()
                  if tea1:
                     tea1.orden=tea.orden
                  else:
                       tea1.orden=1
                  tea1.save()
                  return HttpResponseRedirect(self.url_redirect)
               else:
                    data['error1'] = form.errors
            else:
                data['error1'] = 'No ha ingresado a ninguna opcion'
        except Exception as e:
            data['error1'] = str(e)
        return redirect(reverse_lazy('estructurae', kwargs={'pk': request.POST['asignacion'] })) 

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'TIPO DE ASIGNACION'        
        context['action'] = 'searchdata'  
        context['etapas'] = Etapa.objects.all()  
        asignacion=Asignacion.objects.filter(id=self.kwargs['pk']).first()
        context['proyecto'] = asignacion.proyecto.nombre_proyecto
        context['responsable'] = asignacion.personal.nombre + ' ' + asignacion.personal.a_paterno + ' ' + asignacion.personal.a_materno
        context['actividades'] = Actividad.objects.all().order_by('nombre_actividad')      
        context['create_url'] = reverse_lazy('crear_estructurae')
        context['update_url'] = reverse_lazy('modificar_estructurae')
        context['delete_url'] = reverse_lazy('eliminar_estructurae')
        context['id'] = self.kwargs['pk']
        context['horas'] = horasdia(self.request)
        return context

class EstructuraECreateView(CreateView):
    model = TEA
    form_class = TEAForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('estructurae')
    permission_required = ''
    url_redirect = success_url  
   
    def get_form(self):
        form = super(EstructuraECreateView, self).get_form()
        initial_base = self.get_initial() 

        return form

    def post(self, request, *args, **kwargs):
        data = {}
        action = request.POST['action']
        actividades = request.POST.getlist('actividad[]')

        print('nuevo')
        print(action)
        if action == 'add':
           form = self.get_form()
           if form.is_valid():
              tea=form.save(commit=False)
              tea1 = TEA.objects.filter(asignacion=tea.asignacion, etapa=tea.etapa).order_by('-orden').first()
              listactividades = request.POST.getlist('')
              if tea1:
                  tea.orden=tea1.orden + 1                  
              else:
                   tea.orden=1
              orden1 = tea.orden
              for actividad in actividades:     
                  tea2 = TEA(asignacion=tea.asignacion, etapa=tea.etapa, orden = orden1, actividad_id=actividad)
                  tea2.save()  
                  orden1 = orden1 + 1
              return HttpResponseRedirect(reverse_lazy('estructurae', kwargs={'pk': request.POST['asignacion'] }))
           else:
                data['error1'] = form.errors
        else:
                data['error1'] = 'No ha ingresado a ninguna opcion'
 
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super(EstructuraCreateView, self).get_context_data(**kwargs)
        context['title'] = 'Añadir Actividad'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        
        return context

class EstructuraEUpdateView(UpdateView):
    model = TEA
    form_class = TEAForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('listar_actividad')
    permission_required = ''
    url_redirect = success_url  

    def get_context_data(self, **kwargs):
        context = super(EstructuraUpdateView, self).get_context_data(**kwargs)
        context['title'] = 'Modificar Actividad'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

class EstructuraEDeleteView(DeleteView):
    model = TEA
    form_class = TEAForm
    template_name = 'administrador/crear_proyecto.html'    
    success_url = reverse_lazy('estructurae', kwargs={'pk': 1 })
    permission_required = ''
    url_redirect = success_url    

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        success_url = reverse_lazy('estructurae', args={self.kwargs['pk']})
        return super().dispatch(request, *args, **kwargs) 
    
    def post(self, request, *args, **kwargs):
        data = {}
        action = request.POST['action']
        if action == 'add':
           tea = TEA.objects.filter(id=self.kwargs['pk']).first()
           id=tea.asignacion.id
           tea.delete()
           return HttpResponseRedirect(reverse_lazy('estructurae', kwargs={'pk': id}))           
        else:
                data['error1'] = 'No ha ingresado a ninguna opcion'
 
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super(EstructuraEDeleteView, self).get_context_data(**kwargs)
        context['title'] = 'Eliminar Actividad de la estructura'
        reverse_lazy('estructurae', args={self.kwargs['pk']})
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

def actualizar_orden(request):
   try:
        id = request.GET.get('id', '')
        orden = request.GET.get('orden', '')
        direccion = request.GET.get('direccion', '')
        tea = TEA.objects.get(id=id)
        tea1 = TEA.objects.filter(asignacion=tea.asignacion, etapa=tea.etapa).order_by('orden')        
        tea.orden=int(orden)+int(direccion)
        tea.save()
        return HttpResponse('True')
   except:
          return HttpResponse('False') 

class SupervisorListView(ListView):
    model = Personal
    template_name = 'administrador/supervisores.html'
    permission_required = ''
    url_redirect = reverse_lazy('index')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super(SupervisorListView, self).get_queryset().filter(Q(tipoauditor__nombre_tipoauditor__icontains='Supervisor') | Q(tipoauditor__nombre_tipoauditor__icontains='Director') | Q(tipoauditor__nombre_tipoauditor__icontains='Resp aud SEDECA'))

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for proyecto in Proyecto.objects.all():
                    data.append(proyecto.toJSON())            
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'LISTA DE SUPERVISORES'        
        context['action'] = 'searchdata'        
        context['list_url'] = reverse_lazy('listarproysupervisor')
        context['horas'] = horasdia(self.request)


        return context

class ProyectoSupervisorListView(ListView):
    model = Asignacion
    template_name = 'administrador/proyectossupervisor.html'
    permission_required = ''
    url_redirect = reverse_lazy('index')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super(ProyectoSupervisorListView, self).get_queryset().filter(supervisor_id=self.kwargs['pk'])

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for proyecto in Proyecto.objects.all():
                    data.append(proyecto.toJSON())            
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        proyectos = Proyecto.objects.all()
        
        context = super().get_context_data(**kwargs)
        context['title'] = 'LISTA DE SUPERVISORES'        
        context['action'] = 'searchdata'        
        context['create_url'] = reverse_lazy('crear_proyecto')
        context['list_url'] = reverse_lazy('lista_proyecto', kwargs={'pk': 1 })
        context['horas'] = horasdia(self.request)


        return context

def horasdia(request):
    usuario = request.user
    personal = Personal.objects.filter(usuario=usuario).first()
    horastrabajadasd=0
    import datetime
    fecha=datetime.datetime.now()        
    controls=Control.objects.filter(fecha=fecha, asignacion__personal=personal)
    for control in controls:
        horastrabajadasd = horastrabajadasd + control.horas
    return horastrabajadasd

class GestionesListView(ListView):
    model = Gestion
    template_name = 'administrador/gestiones.html'
    permission_required = ''
    url_redirect = reverse_lazy('index')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super(GestionesListView, self).get_queryset().all()

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for gestion in Gestion.objects.all():
                    data.append(gestion.toJSON())            
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Gestiones'      
        context['horas'] = horasdia(self.request)
        return context

class GestionCreateView(CreateView):
    model = Gestion
    template_name = 'administrador/nuevagestion.html'
    permission_required = ''
    url_redirect = reverse_lazy('index')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super(GestionesListView, self).get_queryset().all()

    def post(self, request, *args, **kwargs):
        data = {}
        action = request.POST['action']
        if action == 'add':
           form = self.get_form()
           if form.is_valid():
              form.save()              
              return HttpResponseRedirect(reverse_lazy('creargestion'))
           else:
                data['error1'] = form.errors
        else:
                data['error1'] = 'No ha ingresado a ninguna opcion'
 
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Gestiones'      
        context['horas'] = horasdia(self.request)
        return context

class CambiarGestionUpdateView(ListView):
    model = Gestion
    template_name = 'administrador/cambiargestion.html'
    permission_required = ''
    url_redirect = reverse_lazy('index')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super(CambiarGestionUpdateView, self).get_queryset().all()

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for gestion in Gestion.objects.all():
                    data.append(gestion.toJSON())            
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Cambiar gestiones'      
        context['horas'] = horasdia(self.request)
        return context
def cambiar(request, pk):
    gestion = Gestion.objects.filter(id=pk).first()
    if gestion:
       request.session['gestion'] = gestion.gestion
    else:
         request.session['gestion'] = 2021
    return HttpResponseRedirect("/")

class AmpliacionesListView(ListView):
    model = Ampliacion
    template_name = 'administrador/ampliacion.html'
    permission_required = ''
    url_redirect = reverse_lazy('index')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super(AmpliacionesListView, self).get_queryset().all()

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for gestion in Gestion.objects.all():
                    data.append(gestion.toJSON())            
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Cambiar gestiones'  
        context['create_url'] = reverse_lazy('crear_ampliacion')    
        return context

class AmpliacionesCreateView(CreateView):
    model = Ampliacion
    form_class = AmpliacionForm
    template_name = 'administrador/crear_ampliacion.html'
    success_url = reverse_lazy('ampliacion')
    permission_required = ''
    url_redirect = success_url  
   
    def get_form(self):
        form = super(AmpliacionesCreateView, self).get_form()
        initial_base = self.get_initial() 

        return form

    def post(self, request, *args, **kwargs):
        data = {}
        action = request.POST['action']
        print("hola")
        print(self.get_form())
        if action == 'add':
           form = self.get_form()
           print(form)
           if form.is_valid():
              form.save()              
              return HttpResponseRedirect(reverse_lazy('ampliacion'))
           else:
                data['error1'] = form.errors
        else:
                data['error1'] = 'No ha ingresado a ninguna opcion'
 
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super(AmpliacionesCreateView, self).get_context_data(**kwargs)
        context['title'] = 'Añadir Ampliacion'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        
        return context

def buscar_trabajo(request):
    xauditoria = request.GET.get('xtrabajo','')
    asignaciones = Asignacion.objects.filter(proyecto__nombre_proyecto__icontains=xauditoria).values('id','proyecto__nombre_proyecto', 'personal__nombre', 'personal__a_paterno', 'personal__a_materno', 'personal__tipoauditor__nombre_tipoauditor') 
    if asignaciones:
       return HttpResponse(  json.dumps(
                       list(asignaciones)
                ), content_type = 'application/json')
    else:
        return HttpResponse(json.dumps(
                        [
                            {
                                'id': 0,
                                'proyecto__nombre_proyecto': "XXXXXXXX",                                 
                            },
                        ]
                ), content_type = 'application/json')

class AsignacionMemoListView(ListView):
    model = Asignacion
    template_name = 'administrador/asignacionmemo.html'
    permission_required = ''
    url_redirect = reverse_lazy('index')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    def get_queryset(self):
        return super(AsignacionMemoListView, self).get_queryset().filter(memo=True, gestion = self.request.session['gestion'])

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for asignacion in Asignacion.objects.all():
                    data.append(asignacion.toJSON())            
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'ASIGNACION DE PROYECTOS'        
        context['action'] = 'searchdata'        
        context['create_url'] = reverse_lazy('crear_asignacionmemo')
        context['list_url'] = reverse_lazy('lista_asignacionesmemo')
        context['horas'] = horasdia(self.request)
        context['asignacionesp'] = Asignacion.objects.filter(poa=True).filter(proyecto__fecha_inicio__year=self.request.session['gestion'])
        context['asignacionesnp'] = Proyecto.objects.filter(tipo='ANP')
        return context

class AsignacionMemoCreateView(CreateView):
    model = Asignacion
    form_class = AsignacionMemoForm
    template_name = 'administrador/crear_asignacionmemo.html'
    success_url = reverse_lazy('listar_asignacionmemo')
    permission_required = ''
    url_redirect = success_url    

    def get_form(self):
        form = super(AsignacionMemoCreateView, self).get_form()
        initial_base = self.get_initial()        
        return form

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                proyecto = Proyecto.objects.filter(id=request.POST.get('proyecto','')).first()                
                if form.is_valid():
                    asignacion = form.save(commit=False)
                    asignacion.proyecto_id=request.POST.get('proyecto','')
                    asignacion.gestion=self.request.session['gestion']
                    asignacion.poa=False
                    asignacion.memo=True
                    asignacion.save()
                    print(request.POST.get('proyecto',''))
                    return HttpResponseRedirect(self.url_redirect)
                else:
                    data['error'] = form.errors
            else:
                data['error'] = 'No ha ingresado a ninguna opcion'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super(AsignacionMemoCreateView, self).get_context_data(**kwargs)
        context['title'] = 'Añadir Asignacion'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)      
        return context

class AsignacionMemoUpdateView(UpdateView):
    model = Asignacion
    form_class = AsignacionMemoForm
    template_name = 'administrador/crear_asignacionmemo.html'
    success_url = reverse_lazy('listar_asignacionmemo')
    permission_required = ''
    url_redirect = success_url  
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                if form.is_valid():
                    print(self.kwargs['pk'])
                    asignacion = form.save(commit=False)
                    asignacion1 = Asignacion.objects.get(id=self.kwargs['pk'])
                    if asignacion1.personal == asignacion.personal and asignacion1.supervisor == asignacion.supervisor:
                       asignacion1.memo = True
                       print('Activando asignacion')
                       asignacion1.nro_memorandum = asignacion.nro_memorandum
                       asignacion1.numero_memorandum_supervisor = asignacion.numero_memorandum_supervisor
                       asignacion1.gestion_memorandum = asignacion.gestion_memorandum
                       asignacion1.fecha_memorandum = asignacion.fecha_memorandum
                       asignacion1.fecha_memorandum_supervisor = asignacion.fecha_memorandum_supervisor
                       asignacion1.plazo = asignacion.plazo
                       asignacion1.plazo_supervisor_memo = asignacion.plazo_supervisor_memo
                       asignacion1.supervisor = asignacion.supervisor                                              
                       asignacion1.save()
                    else:   
                        print("Asignacion: ")
                        print(self.kwargs['pk'])
                        asignacion1.memo = True
                        print('Activando asignacion')
                        asignacion1.save()
                        if asignacion1.personal != asignacion.personal:
                           asignacionauditorant = AsignacionAuditor.objects.filter(asignacion_id=self.kwargs['pk'], estado=True).first()
                           asignacionauditorant.estado = False
                           asignacionauditorant.save()
                           asignacionauditor = AsignacionAuditor(personal=asignacion.personal, cargo=asignacion.cargo, plazo=asignacion.plazo, tipoauditor=asignacion.tipoauditor, nro_memorandum=asignacion.nro_memorandum, fecha_memorandum=asignacion.fecha_memorandum, asignacion=asignacion1, estado=True)
                           asignacionauditor.save()

                        if asignacion1.supervisor != asignacion.supervisor:                     
                           asignacionsupervisorant = AsignacionSupervisor.objects.filter(asignacion_id=self.kwargs['pk'], estado=True).first()
                           asignacionsupervisorant.estado = False
                           asignacionsupervisorant.save()
                           asignacionsupervisor = AsignacionSupervisor(supervisor=asignacion.supervisor, plazo_supervisor_poa=asignacion.plazo_supervisor_poa, plazo_supervisor_memo=asignacion.plazo_supervisor_memo, numero_memorandum_supervisor=asignacion.numero_memorandum_supervisor, fecha_memorandum_supervisor=asignacion.fecha_memorandum_supervisor, supervisorpoa=asignacionsupervisorant.supervisorpoa, asignacion=asignacion1, estado=True)
                           asignacionsupervisor.save()                        

                    return HttpResponseRedirect(self.url_redirect)
                else:
                    data['error'] = form.errors
            else:
                data['error'] = 'No ha ingresado a ninguna opcion'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)
    def get_context_data(self, **kwargs):
        context = super(AsignacionMemoUpdateView, self).get_context_data(**kwargs)
        context['title'] = 'Asignar proyecto a personal segun memorandum'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        
        asignacion = Asignacion.objects.get(id=self.kwargs['pk'])
        reasignacion = Reasignacion.objects.filter(nuevaasignacion=asignacion).last()
        nombre = '' 
        a_paterno = ''
        a_materno = ''
        nombre1 = '' 
        a_paterno1 = ''
        a_materno1 = ''
        if reasignacion:
           if reasignacion.asignacion.poa == True:
                if reasignacion.asignacion.personal.nombre:
                   nombre = reasignacion.asignacion.personal.nombre
                if reasignacion.asignacion.personal.a_paterno:
                   a_paterno = reasignacion.asignacion.personal.a_paterno
                if reasignacion.asignacion.personal.a_materno:
                   a_materno = reasignacion.asignacion.personal.a_materno            
                if reasignacion.asignacion.supervisor:
                   nombre1 = reasignacion.asignacion.supervisor.nombre
                   if reasignacion.asignacion.supervisor.a_paterno:
                      a_paterno1 = reasignacion.asignacion.supervisor.a_paterno
                   if reasignacion.asignacion.personal.a_materno:
                      a_materno1 = reasignacion.asignacion.supervisor.a_materno               
        else:
             if asignacion.poa == True:
                if asignacion.personal.nombre:
                   nombre = asignacion.personal.nombre
                if asignacion.personal.a_paterno:
                   a_paterno = asignacion.personal.a_paterno
                if asignacion.personal.a_materno:
                   a_materno = asignacion.personal.a_materno            
                if asignacion.supervisor:
                   nombre1 = asignacion.supervisor.nombre
                   if asignacion.supervisor.a_paterno:
                      a_paterno1 = asignacion.supervisor.a_paterno
                   if asignacion.personal.a_materno:
                      a_materno1 = asignacion.supervisor.a_materno              
        context['auditor'] = nombre + ' ' + a_paterno + ' ' + a_materno      
        context['supervisor'] = nombre1 + ' ' + a_paterno1 + ' ' + a_materno1
        
        return context

class AsignacionMemoDeleteView(DeleteView):
    model = Asignacion
    form_class = AsignacionMemoForm
    template_name = 'administrador/crear_proyecto.html'
    success_url = reverse_lazy('listar_asignacionmemo')
    permission_required = ''
    url_redirect = success_url    

    def get_context_data(self, **kwargs):
        context = super(AsignacionMemoDeleteView, self).get_context_data(**kwargs)
        context['title'] = 'Eliminar asignacion'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['horas'] = horasdia(self.request)
        return context

def reporteresumenproyecto(request):
    xproyecto_id = request.GET.get('proyecto_id','')    
    reportemes=Reportes.objects.filter(id=5).first()
    report = ReporteResumenProyecto(reportemes, xproyecto_id)                  
    return report.render_to_response()

def listaproyectosresum(request):
    personal=Personal.objects.filter(usuario=request.user).first()          
    if request.user.is_superuser or (personal and personal.tipoauditor.nombre_tipoauditor.find('Supervisor')>-1):
       proyectos = Asignacion.objects.all() 
    else:
         proyectos = Asignacion.objects.filter(personal__id=request.user.id)
    return render(request,"administrador/resumenauditoria.html", { 'object_list' : proyectos}) 

def reportesgenerales(request):
    
    return render(request,"administrador/reportesgenerales.html") 

def reportrelacioncargos(request):
    if request.method == 'GET' and request.GET.get('GESTION',''):
       GESTION = request.GET.get('GESTION','') 
       reportemes=Reportes.objects.filter(id=5).first()
       report = ReporteRelacionCargos(reportemes, GESTION)                  
       return report.render_to_response()

    return render(request,"administrador/reportrelacioncargos.html") 


class TiempoDisponibleUpdate(UpdateView):
    model = TiempoDisponible
    form_class = TiempoDisponibleForm
    template_name = 'administrador/crear_auditor.html'
    success_url = reverse_lazy('tiempodisponible')
    permission_required = ''
    url_redirect = success_url   
    
    def post(self, request, *args, **kwargs):
        data = {}
        tiempodisponible = TiempoDisponible.objects.filter(mes=request.POST.get('mes',''), gestion=request.POST.get('gestion','')).first() 
        form = TiempoDisponibleForm(request.POST, instance=tiempodisponible)
        if form.is_valid():              
           form.save()
           return HttpResponseRedirect(reverse_lazy('tiempodisponible', kwargs={'pk': '2021'}))  
        else:
             data['error1'] = form.errors
          
        return JsonResponse(data)

def reportpoaaudi(request):
    if request.method == 'POST' and request.POST.get('GESTION',''):
       GESTION = request.POST.get('GESTION','') 
       reportemes=Reportes.objects.filter(id=5).first()
       report = ReportePoaAudi(reportemes, GESTION)                  
       return report.render_to_response()

    return render(request,"administrador/reportpoaaudi.html") 

