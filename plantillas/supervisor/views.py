from django.shortcuts import render, redirect
from auditor.views import *
from django.http import HttpResponse, JsonResponse, Http404
import io
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter, landscape
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.enums import TA_CENTER, TA_RIGHT
from reportlab.platypus import Paragraph
from administrador.views import horasdia
import datetime
import json
from datetime import datetime
from json import dumps
from administrador.reportes import *


# Create your views here.
class ProyectoAuditorListView(ListView):
    model = Asignacion
    template_name = 'supervisor/listarproyectossup.html'
    permission_required = ''
    url_redirect = reverse_lazy('supervisor/registro')   
    def get(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
           return HttpResponseRedirect('/administrador/listarsupervisores')
        else:
            object_list = Asignacion.objects.filter(supervisor__usuario=self.request.user)
            datos = Personal.objects.filter(usuario=self.request.user).first()
            usuario=datos.id
            return render(request, 'supervisor/listarproyectossup.html', {'object_list' : object_list, 'supervisor_id': usuario})
    def get_queryset(self):
        return super(ProyectoAuditorListView, self).get_queryset().filter(supervisorpoa__usuario=self.request.user)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
               form=TEAForm(request.POST)
               if form.is_valid():
                  form.save()
                  return HttpResponseRedirect(self.url_redirect)
               else:
                    data['error1'] = form.errors
            else:
                data['error1'] = 'No ha ingresado a ninguna opcion'
        except Exception as e:
            data['error1'] = str(e)
        return redirect(reverse_lazy('estructura', kwargs={'pk': request.POST['tipoasignacion'] })) 

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'TIPO DE ASIGNACION'        
        context['action'] = 'searchdata'  
        context['etapas'] = Etapa.objects.all()      
        context['actividades'] = Actividad.objects.all().order_by('nombre_actividad')      
        context['create_url'] = reverse_lazy('crear_estructura')
        context['update_url'] = reverse_lazy('modificar_estructura')
        context['delete_url'] = reverse_lazy('eliminar_estructura')
        context['horas'] = horasdia(self.request)        
        print("contexto")
        return context

class ControlAuditorListView(ListView):
    model = ControlSupervisor
    template_name = 'supervisor/controlinternosuper.html'
    permission_required = ''
    url_redirect = reverse_lazy('controldiariosup')
    anio = 2021
    mes = 1
    responsable = ''
    trabajo = ''
    pkresp = ''
    pktrabajo=''
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def get_queryset(self):
        today = date.today()
        if self.kwargs['mes'] and int(self.kwargs['mes'])>0:
           mes = self.kwargs['mes'] 
           anio = self.request.session['gestion'] 
        else:    
            mes = today.month
            anio = self.request.session['gestion'] 
        self.mes = mes
        self.anio = anio
        supervisor = Personal.objects.filter(id=self.kwargs['pk']).first()        
        if supervisor:
           self.pkresp = self.kwargs['pk'] 
           self.responsable = supervisor.nombre + ' ' + supervisor.a_paterno + ' ' + supervisor.a_materno
        else:
             self.pkresp = self.kwargs['pk'] 
             self.pktrabajo = self.kwargs['trabajo']

        
        return super(ControlAuditorListView, self).get_queryset().filter(asignacion__asignacionsupervisor__supervisor_id=self.kwargs['pk'], asignacionsupervisor__estado=True).filter(fecha__year = anio, fecha__month = mes)
    
    def post(self, request, *args, **kwargs):
        data = {}
        print("guardar")
        try:
            horas = request.POST.getlist('horas[]')
            asignaciones=request.POST.getlist('asignacion[]')
            fechas=request.POST.getlist('fecha[]')
            print('Fechas')
            etapas=request.POST.getlist('etapa[]')
            i = 0
            
            for etapa in etapas:
                asignacion = Asignacion.objects.filter(id=asignaciones[i]).first()
                asignacionsupervisor = AsignacionSupervisor.objects.filter(asignacion=asignacion, estado=True).first()    
                etapa = Etapa.objects.filter(id=etapas[i]).first()                
                if horas[i]:
                   controlsupervisor = ControlSupervisor.objects.filter(asignacion = asignacion, etapa = etapa, fecha=fechas[i], asignacionsupervisor=asignacionsupervisor).first()
                   if controlsupervisor:
                      controlsupervisor.horas=horas[i]
                      print("Registro actualizado")
                   else:
                        print("Registro nuevo")
                        controlsupervisor = ControlSupervisor(asignacion = asignacion, etapa = etapa, fecha=fechas[i], horas=horas[i], asignacionsupervisor=asignacionsupervisor)
                   controlsupervisor.save()
                i = i + 1
            return HttpResponseRedirect(reverse_lazy('controldiariosup', kwargs={'pk': request.POST['supervisor_id'], 'mes': request.POST['mes'], 'anio': request.POST['anio'] }))
               
            
        except Exception as e:
            data['error1'] = str(e)
            print(str(e))
        return redirect(reverse_lazy('controldiariosup', kwargs={'pk': request.POST['supervisor_id'], 'mes': request.POST['mes'], 'anio': request.POST['anio'] })) 

    def lista_meses(self):
        import datetime      
        array1 = []
        meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'] 
        array = []
        i = 0
        for mes1 in meses:      
            mesc = {
              'mes_str' : meses[i],
              'mes'     : str(i + 1)}
            i = i + 1
            array1.append(mesc)
        return array1

    def lista(self):  
        import datetime      
        array1 = []
        dias_mes = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]; 
        dias_semana = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];                 
        for dia in range(1,dias_mes[int(self.mes) - 1] + 1):            
            dia = datetime.datetime(int(self.anio), int(self.mes), dia)            
            if (dia.weekday()<=5):
               diasem = dias_semana[dia.weekday()+1][:2]
            else:
                diasem = dias_semana[0][:2]
            dia = {
               'dia_semana':diasem,
               'dia':dia.day}
            array1.append(dia)
        print(array1)
        return array1

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['dias'] = self.lista()
        context['action'] = 'searchdata'  
        context['responsable'] = self.responsable  
        mes_anio = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]   
        context['mes_str'] = mes_anio[int(self.mes) - 1]      
        context['anio_str'] = self.anio                 
        context['trabajo'] = self.trabajo
        self.mes_anio = mes_anio
        context['meses'] = self.lista_meses()
        context['pkresp'] = self.pkresp
        context['pktrabajo'] = self.pktrabajo
        context['mes'] = str(self.mes)
        context['horas'] = horasdia(self.request)
        context['etapas'] = Etapa.objects.all()
        context['asignaciones'] = Asignacion.objects.filter(supervisor__id=self.kwargs['pk']).order_by('proyecto_id')
        context['supervisor_id'] = self.kwargs['pk']
        
        
        return context

class ControlMesListView(ListView):
    model = Asignacion
    template_name = 'supervisor/reportesmes.html'
    permission_required = ''
    url_redirect = reverse_lazy('auditor/registro')
    anio = 2021
    mes = 1
    responsable = ''
    trabajo = ''
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def get_queryset(self):
        if self.request.user.is_superuser:
           asignacion = Asignacion.objects.all()
           return super(ControlMesListView, self).get_queryset().all()
        else:
             if self.request.user.groups.filter(name='SUPERVISOR'): 
                asignacion = Asignacion.objects.filter(supervisor__usuario=self.request.user)
                return super(ControlMesListView, self).get_queryset().filter(supervisor__usuario=self.request.user)
             else:
                  asignacion = Asignacion.objects.filter(personal__usuario=self.request.user)
                  return super(ControlMesListView, self).get_queryset().filter(personal__usuario=self.request.user)
    
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
               form=TEAForm(request.POST)
               if form.is_valid():
                  form.save()
                  return HttpResponseRedirect(self.url_redirect)
               else:
                    data['error1'] = form.errors
            else:
                data['error1'] = 'No ha ingresado a ninguna opcion'
        except Exception as e:
            data['error1'] = str(e)
        return redirect(reverse_lazy('estructura', kwargs={'pk': request.POST['tipoasignacion'], 'mes': request.POST['tipoasignacion'], 'dia': request.POST['tipoasignacion'] })) 
    
    def lista(self):  
        import datetime      
        array1 = []
        dias_mes = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]; 
        dias_semana = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];         
        for dia in range(1,dias_mes[int(self.mes) - 1]):
            dia = datetime.datetime(int(self.anio), int(self.mes), dia)            
            diasem = dias_semana[dia.weekday()][:2]
            dia = {
               'dia_semana':diasem,
               'dia':dia.day}
            array1.append(dia)
        return array1

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action'] = 'searchdata'  
        context['horas'] = horasdia(self.request)
        return context

def reportemes(request, pk):
    from django.db.models import Count
    control = Control.objects.values('tea__etapa__nombre_etapa', 'tea__etapa__id').annotate(dcount=Count('tea__etapa__nombre_etapa')).order_by('tea__etapa__id')
    asignacion = Asignacion.objects.all()
    mes = 4
    con = 0
    def headPdf(c):
        from datetime import datetime
        control1 = Control.objects.all().order_by('tea__etapa__id').first()
        image = '/static/images/logo.jpg'
        image2 = '/static/images/escudo.jpg'
        # images
        #c.drawImage(image, 340, 730, width=30, height=30)
        c.drawImage(image2, 10, 550, width=40, height=35)
        c.setLineWidth(.3)
        c.setFont('Helvetica-Oblique', 8)
        c.drawString(50, 570, 'GOBIERNO AUTÓNOMO DEPARTAMENTAL DE POTOSÍ')
        c.drawString(55, 560, 'DIRECCION DEPARTAMENTAL DE AUDITORIA INTERNA')
        c.setFont('Helvetica-Bold', 12)
        months = ("Enero", "Febrero", "Marzo", "Abri", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
        c.drawString(500, 750, months[1]+' de '+str('2021'))
        c.setFont('Helvetica-Bold', 14)
        c.drawString(240, 530, "CONTROL DIARIO DE HORAS DE TRABAJO")
        c.setFont('Helvetica', 10)
        fecha=datetime(2021, 1, 1)
        day = fecha.day
        month = months[fecha.month - 1]
        year = fecha.year
        message = "{} de {} del {}".format(day, month, year)
        c.drawString(30, 730, 'Potosí, '+message)
        titulo1 = 'Ing.'

        texto = 'Apellidos y nombres del supervisor: '
        print(control1)
        supervisor = titulo1 + ' '+ control1.asignacion.supervisor.nombre+ ' '+control1.asignacion.supervisor.a_paterno+ ' '+control1.asignacion.supervisor.a_materno
        texto1 = 'IDENTIFICACION DEL TRABAJO:'
        texto2 = 'Trabajo mes de' 
        c.setFont('Helvetica-Bold', 9)  
        c.drawString(30, 485, texto)
        c.setFont('Helvetica', 9)  
        c.drawString(200, 485, supervisor)
        c.setFont('Helvetica-Bold', 9)  
        c.drawString(420, 485, texto1)
        c.setFont('Helvetica', 9)  
        c.drawString(570, 485, texto2 + ' ' + months[mes])
        
        c.setFont('Helvetica-Bold', 9)  
        c.drawString(20, 465, 'RESPON')
        c.drawString(20, 455, 'SABLE')
        

        c.drawString(60, 465, 'ETAPA')
        c.drawString(60, 455, ' DE AUD.')

        c.drawString(110, 465, 'No')
        c.drawString(160, 465, 'ACTIVIDADES')
        c.drawString(360, 465, 'REVISION')
        c.drawString(360, 455, 'FECHA')
        c.drawString(360, 445, 'DIA')

        c.line(19, 476, 780, 476)
        c.line(19, 440, 770, 440)
        c.line(19, 476, 19, 10)
        c.line(770, 476, 770, 10)
        c.line(19, 10, 770, 10)
        c.line(59, 476, 59, 10)
        c.line(109, 476, 109, 10)
        c.line(129, 476, 129, 10)
        c.line(349, 476, 349, 10)
        c.line(412, 476, 412, 10)
        c.setFont('Helvetica', 9)
        # DIBUJAR DIAS EN TABLA
        columna = 0
        dias=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
        dias_lab=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
        dias_semana = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"]         
        anio = 2021
        
        import datetime
        for dia in range(1,30):
            columna = columna + 1           
            dia = datetime.datetime(int(anio), int(mes), columna)            
            diasem = dias_semana[dia.weekday()][:2]
            c.setFont('Helvetica', 8) 
            c.drawString(406 + columna * 12, 445, diasem)
            c.setFont('Helvetica', 8)
            c.drawString(407 + columna * 12, 455, str(dias_lab[columna - 1]))
            c.setFont('Helvetica', 6)
            c.drawString(408 + columna * 12, 465, str(dias[columna - 1]))
            c.setFont('Helvetica', 9)
            c.line(417+columna*12, 476, 417+columna*12, 10)
        c.setFont('Helvetica', 9)  

        # start 170, height end y, height  
    def cargar_tabla():
        row = []
        tabla = []

    def dibujar_table(c, controll):
         # DIBUJAR TABLA
         c.rotate(90)
         text = "RESPONSABLE DE LA EJECUCION"
         c.drawString(200, -40, text)
         c.rotate(-90)       
         fila = 0   
         fila_pro=0
         for etapas in controll:
             fila = fila + 1
             c.rotate(90)
             text = etapas['tea__etapa__nombre_etapa']
             c.drawString(500 - (fila * 130) , -72, text)
             c.rotate(-90)
             control2 = Control.objects.filter(tea__etapa__id=etapas['tea__etapa__id']).order_by('asignacion__proyecto__nombre_proyecto').values('asignacion__proyecto__nombre_proyecto', 'asignacion__proyecto__id').annotate(dcount=Count('asignacion__proyecto__nombre_proyecto')).order_by('asignacion__proyecto__nombre_proyecto')
             c.line(59, 450-fila*120, 770, 450-fila*120)
             no_proyecto = 0
             for proyecto in control2:
                 fila_pro=fila_pro + 1
                 no_proyecto = no_proyecto + 1 
                 c.drawString(116, 540-(fila_pro)*8-fila*100, str(no_proyecto))
                 c.setFont('Helvetica', 7)  
                 c.drawString(140, 540-(fila_pro)*8-fila*100, proyecto['asignacion__proyecto__nombre_proyecto'])
                 c.setFont('Helvetica', 7)  
                 c.drawString(360, 540-(fila_pro)*8 -fila*100 , 'JESUS')

    def datos_generales(c):
        c.line(30, 525, 750, 525)
        c.setFont('Helvetica', 9)
        

    def style_table_size(c):
        width, height = letter
        table = Table(data, colWidths=[1.3 * cm, 12.7 * cm, 2.2 * cm, 3 * cm, 3 * cm, 3.5 * cm])
        table.setStyle(TableStyle([
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ]))

        table.wrapOn(c, width, height)
        table.drawOn(c, 30, high)

    # Create the HttpResponse headers with PDF
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=reporte_mensual.pdf'
    # Create pdf object
    buffer = io.BytesIO()
    c = canvas.Canvas(buffer, pagesize=(landscape(letter)))
    headPdf(c)
    dibujar_table(c, control)
    # Table header styles
    styles = getSampleStyleSheet()
    styleBH = styles["Normal"]
    styleBH.alignment = TA_CENTER
    styleBH.fontSize = 9
    # table content
    styles = getSampleStyleSheet()
    styleBA = styles["Normal"]
    styleBA.alignment = TA_RIGHT
    styleBA.fontSize = 8
    styles = getSampleStyleSheet()
    styleBB = styles["Normal"]
    styleBB.alignment = TA_CENTER
    styleBB.fontSize = 8   
    # Table
    high = 470
    data = []
    temporal = []
    # start 170, height end y, height
    c.save()
    # get the value of BytesIO buffer and write response
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)
    return response

class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj, 'isoformat'): #handles both date and datetime objects
            return obj.isoformat()
        else:
            return json.JSONEncoder.default(self, obj)

class DateTimeEncoder(JSONEncoder):
        #Override the default method
        def default(self, obj):
            if isinstance(obj, (datetime.date, datetime.datetime)):
                return obj.isoformat()

def cargardatossuper(request):
    xsupervisor_id = request.GET.get('supervisor_id','')
    supervisor = Personal.objects.get(id=xsupervisor_id)
    xmes = request.GET.get('xmes','')
    xanio = request.GET.get('xanio','')
    dias_mes = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    desde = xanio + "-" + xmes + "-" + "01" 
    hasta = xanio + "-" + xmes + "-" + str(dias_mes[int(xmes) - 1])
    rango = [desde, hasta]
    controlessupervisor = ControlSupervisor.objects.filter(asignacion__supervisor__id=xsupervisor_id, fecha__range = rango, asignacionsupervisor__estado=True).values('id','asignacion_id', 'etapa_id', 'fecha', 'horas') 
    array=[]
    dato = ()
    if controlessupervisor:
       for controlsupervisor in controlessupervisor:
           dato = {
                                'id': controlsupervisor['id'],
                                'asignacion_id': controlsupervisor['asignacion_id'],                                 
                                'etapa_id': controlsupervisor['etapa_id'],                                 
                                'fecha' : str(controlsupervisor['fecha']),
                                'horas' : controlsupervisor['horas']
                            }
           array.append(dato)
       return HttpResponse(  json.dumps(
                       array
                ), content_type = 'application/json')
    else:
        return HttpResponse(json.dumps(
                        [
                            {
                                'id': 0,
                                'proyecto__nombre_proyecto': "XXXXXXXX",                                 
                            },
                        ]
                ), content_type = 'application/json')

def reportemensualsuper(request):
    xsupervisor_id = request.GET.get('supervisor','')
    supervisor = Personal.objects.get(id=xsupervisor_id)
    xmes = request.GET.get('mes','')
    meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
    xanio = request.GET.get('gestion','')
    reportemes=Reportes.objects.filter(id=5).first()
    report = ReporteMensualSupervisor(reportemes, xmes, xanio, meses[int(xmes)-1], xsupervisor_id)                  
    return report.render_to_response()

def resumenanual(request):
    if request.method == 'GET' and request.GET.get('gestion','') :       
       xgestion = request.GET.get('gestion','') 
       supervisor_id = request.GET.get('supervisor_id','') 
       personal=Personal.objects.filter(id=supervisor_id).first()        
       xauditor = personal.id
       report = ReporteResumenTrabajadorAuditor(personal, xgestion, xauditor)                  
       return report.render_to_response()
    else:
         object_list = Asignacion.objects.all()
         personal=Personal.objects.filter(usuario=request.user).first()        
         xauditor = personal.id
         auditores = Personal.objects.filter(tipoauditor__nombre_tipoauditor__icontains='SUPERVISOR')
    return render(request,"supervisor/resumenanual.html", {'object_list': object_list, 'supervisor_id': xauditor, 'auditores': auditores})  