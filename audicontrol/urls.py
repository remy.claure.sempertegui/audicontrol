"""audicontrol URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views
from .views import *
from django.conf.urls import include
from . import views
from django.conf.urls import url
from django.views.static import serve

urlpatterns = [
    url(r'^administrador/', include('administrador.urls')),    
    url(r'^auditor/', include('auditor.urls')),    
    url(r'^supervisor/', include('supervisor.urls')),    
    url(r'^director/', include('director.urls')),    
    path('',  views.privado, name='privado'),
    url('^accounts/login/$',  views.login), 
    url('^accounts/logout/$',  views.login),        
]
