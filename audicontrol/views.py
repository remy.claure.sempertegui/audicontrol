from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404,render,HttpResponseRedirect
from django.template import RequestContext
from django.contrib.auth.forms import AdminPasswordChangeForm, AuthenticationForm, authenticate, UserCreationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from django.db.models import Q
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.decorators import login_required
#from django.core.urlresolvers import reverse
from django.urls import reverse
from django.forms import ModelForm
from django.views.decorators import *
from django.contrib.sessions.models import Session
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.core import serializers
from datetime import datetime
from django.contrib.auth.models import auth
import json
import os
import time
# Create your views here.
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from administrador.models import Personal, Gestion
from administrador.models import PrincipalPersonal
from administrador.views import horasdia

def index(request):
    if not request.user.is_anonymous():
        return HttpResponseRedirect('/privado')
    else:
        return HttpResponseRedirect('/login/')
@login_required
def privado(request):
	if request.user.is_authenticated:
		usuario=request.user
		if usuario.is_active:
			return render(request,"menu.html", {'horas': horasdia(request)})
		else:
			return HttpResponseRedirect('/')
	else:
		return HttpResponseRedirect('/')

def login(request):
    username = request.POST.get('username', False)
    password = request.POST.get('password', False)
    user = auth.authenticate(username=username, password=password)
    if user is not None and user.is_active:
    #    # Correct password, and the user is marked "active"
        auth.login(request, user)
    #    # Redirect to a success page.        
        request.session['nombre'] = user.first_name+" " +user.last_name
        personal = Personal.objects.filter(usuario=user).first()
        gestion = Gestion.objects.filter(estado='EN EJECUCION').first()
        if gestion:
           request.session['gestion'] = gestion.gestion
        else:
            request.session['gestion'] = 2021

        if personal:
           request.session['usuarioid'] = personal.id
           principal_personal=PrincipalPersonal.objects.using('db_personal').filter(ci__contains=personal.ci.strip()).first()
           if principal_personal:
              request.session['foto_bits'] = principal_personal.foto1
           else:
                request.session['foto_bits'] = ""
                
        else:
            request.session['usuarioid'] = '1'
            request.session['foto_bits'] = ''

        return render(request,"menu.html",  {'horas': horasdia(request)})
    #else:
    #    # Show an error page
    print("prueba")
    return render(request,"registration/login.html")


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/login')