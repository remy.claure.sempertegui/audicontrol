from django import template
register = template.Library()
@register.simple_tag
@register.filter(name='suma_total')
def suma_total(role_total):
    return sum( [d.horas for d in role_total] )
