# pull official base image
FROM python:3.8

# set work directory
WORKDIR /code

# set environment variables
COPY requirements.txt /code/
# install dependencies
RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt
COPY . /code/	

# copy project
