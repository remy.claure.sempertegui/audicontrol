from django.contrib import admin
from django.urls import path
from . import views
from .views import *
from django.conf.urls import include
from . import views
from django.conf.urls import url
from django.views.static import serve
urlpatterns = [
    path('registro/', views.ProyectoAuditorListView.as_view(), name='actualizar_auditor'),
    path('controldiario/<pk>/<mes>/<anio>/', views.ControlAuditorListView.as_view(), name='controldiariosup'),
    path('controlmes/<mes>/', views.ControlMesListView.as_view(), name='controlmes'),
    path('reportemes/<pk>/', views.reportemes, name='reportemes'),
    path('cargardatossuper', views.cargardatossuper, name='cargardatossuper'),
    path('reportemensualsuper', views.reportemensualsuper, name='reportemensualsuper'),
    path('resumenanual/', views.resumenanual, name='resumenanual'),    
]