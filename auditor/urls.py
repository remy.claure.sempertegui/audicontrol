from django.contrib import admin
from django.urls import path
from . import views
from .views import *
from django.conf.urls import include
from . import views
from django.conf.urls import url
from django.views.static import serve
urlpatterns = [
    #url(r'^accounts/', include('registration.backends.simple.urls')),
    path('registro/', views.ProyectoAuditorListView.as_view(), name='actualizar_auditor'),
    path('controldiario/<pk>/<mes>/<dia>/<anio>/<trabajo>', views.ControlAuditorListView.as_view(), name='controldiario'),
    path('iniciartrabajo', views.iniciartrabajo, name='iniciartrabajo'),
    path('reporte_mensual', views.reporte_mensual, name='reporte_mensual'),   
    path('reporte_mensual_param', views.reporte_mensual_param, name='reporte_mensual_param'),   
    path('creardia/', views.creardia, name='reporte_mensual'),  
    path('controlmesg', views.controlmesg, name='controlmesg'),  
    path('controlanualg', views.controlanualg, name='controlanualg'),  
    path('reportresumentrabajoauditor', views.reportresumentrabajoauditor, name='reportresumentrabajoauditor'),  

    
]