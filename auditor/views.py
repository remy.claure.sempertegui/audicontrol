from django.shortcuts import render
from django.views.generic import ListView, UpdateView, CreateView, DeleteView
from administrador.models import *
from administrador.forms import *
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from datetime import date
from datetime import datetime
from django.http import HttpResponseRedirect
from django.contrib.humanize.templatetags.humanize import intcomma
from administrador.reportes import ReporteCostos, ReporteMensual, ReporteMensualAuditor, ReporteAnualAuditor, ReporteResumenTrabajadorAuditor3
from administrador.views import horasdia
from django.db.models import Q
# Create your views here.
class ProyectoAuditorListView(ListView):
    model = Asignacion
    template_name = 'auditor/listarproyectos.html'
    permission_required = ''
    url_redirect = reverse_lazy('auditor/registro')   
    
    def get_queryset(self):
        return super(ProyectoAuditorListView, self).get_queryset().filter(personal__usuario=self.request.user)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
               form=TEAForm(request.POST)
               if form.is_valid():
                  form.save()
                  return HttpResponseRedirect(self.url_redirect)
               else:
                    data['error1'] = form.errors
            else:
                data['error1'] = 'No ha ingresado a ninguna opcion'
        except Exception as e:
            data['error1'] = str(e)
        return redirect(reverse_lazy('estructura', kwargs={'pk': request.POST['tipoasignacion'] })) 

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'TIPO DE ASIGNACION'        
        context['action'] = 'searchdata'  
        context['etapas'] = Etapa.objects.all()      
        context['actividades'] = Actividad.objects.all().order_by('nombre_actividad')      
        context['create_url'] = reverse_lazy('crear_estructura')
        context['update_url'] = reverse_lazy('modificar_estructura')
        context['delete_url'] = reverse_lazy('eliminar_estructura')
        context['horas'] = horasdia(self.request)
        asignaciones = Asignacion.objects.filter(personal__usuario=self.request.user)
        totaldias = 0
        for asignacion in asignaciones:
            if asignacion.plazo:
               totaldias = totaldias + asignacion.plazo
        personal = Personal.objects.filter(usuario=self.request.user).first()
        if personal:
           totaldiashabiles=personal.dias_efectivos()
        else:
             totaldiashabiles = 0

        context['totaldias'] = totaldias   
        context['totaldiashabiles'] = totaldiashabiles 
        return context

class ControlAuditorListView(ListView):
    model = Control
    template_name = 'auditor/controlinterno.html'
    permission_required = ''
    url_redirect = reverse_lazy('auditor/registro')
    anio = 2021
    mes = 1
    dia = 1
    responsable = ''
    trabajo_str = ''
    trabajo = '0'
    auditor_id = 0
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def get_queryset(self):
        import datetime      
        today = date.today()
        if self.kwargs['mes'] and int(self.kwargs['mes'])>0:
           mes = self.kwargs['mes']
        else:    
            mes = today.month
        if self.kwargs['dia'] and int(self.kwargs['dia'])>0:
           dia = self.kwargs['dia']
        else:    
            dia = today.day
        if self.kwargs['anio'] and int(self.kwargs['anio'])>0:
           anio = self.kwargs['anio']
        else:    
           anio = today.year
        self.mes = int(mes)        
        self.dia = int(dia)
        self.anio = int(anio)
        dias_semana = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"]   
        dia1 = datetime.datetime(self.anio, self.mes, self.dia)         
        self.dia_str = dias_semana[dia1.weekday()]
        asignacion = Asignacion.objects.filter(personal__usuario=self.request.user, proyecto__id=self.kwargs['trabajo']).first()
        if asignacion:
           self.responsable = asignacion.personal.tipoauditor #control.asignacion.personal.nombre + ' ' + control.asignacion.personal.a_paterno + ' ' + control.asignacion.personal.a_materno
           self.trabajo_str = asignacion.proyecto.nombre_proyecto
        self.trabajo = self.kwargs['trabajo']
        self.auditor_id = self.kwargs['pk']
        asignacionauditor=AsignacionAuditor.objects.filter(asignacion=asignacion, estado=True).first()

        return super(ControlAuditorListView, self).get_queryset().filter(asignacion__personal__usuario=self.request.user, asignacion__proyecto__id=self.kwargs['trabajo'], asignacionauditor=asignacionauditor).filter(fecha__year = anio, fecha__month = mes, fecha__day = dia).order_by('tea__orden')
    
    def post(self, request, *args, **kwargs):
        data = {}
        trabajo=0
        try:
            action = request.POST['action']
            if action == 'add':
               form=TEAForm(request.POST)
               i=0
               for i in range(1,10):
                   horas = request.POST.getlist('horas'+str(i)+'[]')
                   id_tea=request.POST.getlist('id_tea'+str(i)+'[]')
                   j = 0
                   for hora in horas:
                       if hora:
                           control = Control.objects.filter(id=id_tea[j]).first()
                           control.horas=hora
                           control.save()
                           trabajo = control.asignacion.proyecto.id
                           self.auditor_id = control.asignacion.personal.id
                       j = j + 1

               if form.is_valid():
                  print(form) 
                  form.save()
                  print('auditor')
                  print(self.auditor_id)
                  return HttpResponseRedirect(self.url_redirect)
               else:
                    data['error1'] = form.errors
            else:
                data['error1'] = 'No ha ingresado a ninguna opcion'
        except Exception as e:
            data['error1'] = str(e)
        return HttpResponseRedirect(reverse_lazy('controldiario', kwargs={'pk': self.auditor_id, 'mes':0, 'dia':0, 'anio':2021, 'trabajo':trabajo })) 
    
    def lista(self):  
        import datetime      
        array1 = []
        dias_mes = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31] 
        dias_semana = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"]
        for dia1 in range(1, dias_mes[self.mes - 1]+1):
            dia = datetime.datetime(self.anio, self.mes, dia1)             
            diasem = dias_semana[dia.weekday()]
            dia = {
                  'dia_semana':diasem,
                  'dia':dia.day}
            array1.append(dia)
        return array1
    def lista_hoy(self):
        import datetime      
        array1 = []
        dias_mes = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31] 
        dias_semana = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"] 
        dia = datetime.datetime(self.anio, self.mes, self.dia)         
        diasem = dias_semana[dia.weekday()]
        dia = {
              'dia_semana':diasem,
              'dia':dia.day}
        array1.append(dia)
        return array1
    
    def lista_meses(self):
        import datetime      
        array1 = []
        meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']; 
        array = []
        i = 0
        for mes1 in meses:      
            mesc = {
              'mes_str' : meses[i],
              'mes'     : i + 1}
            i = i + 1
            array1.append(mesc)
        return array1

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['dias'] = self.lista()
        context['action'] = 'searchdata'  
        context['responsable'] = self.responsable  
        mes_anio = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];   
        context['mes_str'] = mes_anio[self.mes - 1]      
        context['mes'] = self.mes      
        context['meses'] = self.lista_meses()
        context['anios'] = [2019, 2020, 2021]
        context['anio_str'] = self.anio      
        context['actividades'] = Actividad.objects.all().order_by('nombre_actividad')    
        context['dia_str'] = self.dia_str      
        context['dia'] = self.dia   
        context['dias_hoy'] = self.lista_hoy()    
        context['trabajo_str'] = self.trabajo_str    
        context['trabajo'] = self.trabajo 
        context['auditor_id'] = self.auditor_id   
        context['horas'] = horasdia(self.request)
        
        return context

def iniciartrabajo(request):
    id = request.GET.get('id', '')
    xfecha = request.GET.get('xfecha', '')
    asignacion= Asignacion.objects.filter(id=id).first()
    asignacion.fecha_inicio=xfecha
    asignacion.estado='EN EJECUCION'
    asignacion.save()
    return HttpResponseRedirect('/auditor/registro')

def reporte_mensual_param(request):
    personal = Personal.objects.filter(usuario=request.user).first()       
    auditores = Personal.objects.all()   
    asignaciones = Asignacion.objects.filter(personal_id=personal.id)
    return render(request,"auditor/reportemensualparam.html", { 'personal' : personal, 'auditores' : auditores, 'asignaciones': asignaciones })  

def reporte_mensual(request):
    reportemes=Reportes.objects.filter(id=5).first()
    xparametro1 = request.GET.get('xparametro1','') 
    xparametro2 = request.GET.get('xparametro2','') 
    xparametro3 = request.GET.get('xparametro3','') 
    xparametro4 = request.GET.get('xparametro4','') 
    xparametro5 = request.GET.get('xparametro5','') 
    print(xparametro1)
    print(xparametro2)
    print(xparametro3)
    print(xparametro4)
    print(xparametro5)


    report = ReporteMensual(reportemes, xparametro1, xparametro2, xparametro3, xparametro4, xparametro5)                  
    return report.render_to_response()

def creardia(request):
    dia=request.POST.get('dian', '')
    mes=request.POST.get('mesn', '')
    anio=request.POST.get('anion', '')
    trabajo_id=request.POST.get('trabajo_id', '')
    auditor_id=request.POST.get('auditor_id', '')    

    asignacion = Asignacion.objects.filter(personal_id=auditor_id, proyecto_id=trabajo_id).first()
    asignacionauditor = AsignacionAuditor.objects.filter(asignacion=asignacion, estado=True).first()
    teas=TEA.objects.filter(asignacion = asignacion)
    import datetime
    fecha = datetime.date(int(anio), int(mes), int(dia))
    for tea in teas:
        print(asignacionauditor)
        control = Control(asignacion=asignacion, tea=tea, fecha=fecha, horas = 0, estado = False, asignacionauditor=asignacionauditor)
        control.save()  
         

    return HttpResponseRedirect('/auditor/controldiario/'+auditor_id+'/'+mes+'/'+dia+'/'+anio+'/'+trabajo_id)

def controlmesg(request):
    if request.method == 'POST':
       xmes = request.POST.get('xmes','') 
       xgestion = request.POST.get('xgestion','') 
       meses = ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE','NOVIEMBRE','DICIEMBRE']
       personal=Personal.objects.filter(usuario=request.user).first()          
       if request.user.is_superuser or (personal and personal.tipoauditor.nombre_tipoauditor.find('Supervisor')>-1):
          xauditor = request.POST.get('xpersonal','')
       else:
            xauditor = personal.id       
       gestion = Gestion.objects.all()
       report = ReporteMensualAuditor(gestion, xmes, meses[int(xmes)-1], xauditor, xgestion)                  
       return report.render_to_response()
    personal = Personal.objects.filter(usuario = request.user).first()
    
    if personal:
        x=1
    else:
         personal = Personal.objects.get(username = 'admin') 
    auditores = Personal.objects.all()
    return render(request,"auditor/reportemensualg.html", { 'personal': personal, 'auditores' : auditores })  

def controlanualg(request):
    if request.method == 'GET' and request.GET.get('xproyecto_id',''):
       xgestion = request.GET.get('xanio','') 
       xproyecto_id = request.GET.get('xproyecto_id','') 
       personal=Personal.objects.filter(usuario=request.user).first()        
       xauditor = personal.id
       gestion = Gestion.objects.all()
       report = ReporteAnualAuditor(gestion, xauditor, 2020, xproyecto_id)                  
       return report.render_to_response()
    else:
         asignacion = Asignacion.objects.filter(personal__usuario=request.user)
    return render(request,"auditor/reporteanualg.html", { 'object_list' : asignacion}) 

def reportresumentrabajoauditor(request):
    if request.method == 'GET' and request.GET.get('xgestion',''):
       xgestion = request.GET.get('xgestion','') 
       xauditor = request.GET.get('xpersonal','') 
       personal=Personal.objects.filter(usuario=request.user).first()        
       if not request.user.is_superuser:
          if  (personal and personal.tipoauditor.nombre_tipoauditor.find('Supervisor')>-1):
              print('Supervisor')
          else:
               print('auditor')
               xauditor = personal.id
       gestion = Gestion.objects.all()
       report = ReporteResumenTrabajadorAuditor3(gestion, 2021, xauditor)                  
       print(xauditor)
       return report.render_to_response()
    else:
         personal=Personal.objects.filter(usuario=request.user).first()        
         asignacion = Asignacion.objects.filter(personal__usuario=request.user)
         auditores = Personal.objects.filter(Q(tipoauditor__nombre_tipoauditor__icontains='Auditor') | Q(tipoauditor__nombre_tipoauditor__icontains='Resp') | Q(tipoauditor__nombre_tipoauditor__icontains='Consultor')).order_by('nombre')
    return render(request,"auditor/reporteanualgauditor.html", { 'personal' : personal, 'auditores': auditores }) 




