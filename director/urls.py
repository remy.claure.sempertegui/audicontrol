from django.contrib import admin
from django.urls import path
from . import views
from .views import *
from django.conf.urls import include
from . import views
from django.conf.urls import url
from django.views.static import serve
urlpatterns = [
    #url(r'^accounts/', include('registration.backends.simple.urls')),
    path('registro/', views.ProyectoDirectorListView.as_view(), name='actualizar_director'),  
    path('controldiariodir/<rango>/<mes>/<anio>/', views.ControlDirectorListView.as_view(), name='controldiariodir'), 
    path('cargardatosdir', views.cargardatosdir, name='cargardatosdir'),  
    path('reportemensualdirector/', views.reportemensualdirector, name='reportemensualdirector'),
]