from django.shortcuts import render, redirect
from django.views.generic import ListView, UpdateView, CreateView, DeleteView
from django.http import HttpResponse, JsonResponse, Http404
import io
import json
from administrador.models import *
from administrador.forms import *
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from datetime import date
from datetime import datetime
from django.http import HttpResponseRedirect
from django.contrib.humanize.templatetags.humanize import intcomma
from administrador.reportes import ReporteCostos, ReporteMensual, ReporteMensualAuditor, ReporteAnualAuditor, ReporteControlDiarioDirector
from administrador.views import horasdia

# Create your views here.

class ProyectoDirectorListView(ListView):
    model = Asignacion
    template_name = 'director/listarproyectosdir.html'
    permission_required = ''
    url_redirect = reverse_lazy('director/registro')   
    def get(self, request, *args, **kwargs):
        object_list = Proyecto.objects.all().order_by('nombre_proyecto')
        return render(request, 'director/listarproyectosdir.html', {'object_list' : object_list })
    def get_queryset(self):
        return super(ProyectoDirectorListView, self).get_queryset().all()
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']           
        except Exception as e:
            data['error1'] = str(e)
        return redirect(reverse_lazy('estructura', kwargs={'pk': request.POST['tipoasignacion'] })) 

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'TIPO DE ASIGNACION'        
        context['action'] = 'searchdata'  
        context['etapas'] = Etapa.objects.all()      
        context['actividades'] = Actividad.objects.all().order_by('nombre_actividad')      
        context['create_url'] = reverse_lazy('crear_estructura')
        context['update_url'] = reverse_lazy('modificar_estructura')
        context['delete_url'] = reverse_lazy('eliminar_estructura')
        context['horas'] = horasdia(self.request)        
        print("contexto")
        return context

class ControlDirectorListView(ListView):
    model = ControlDirector
    template_name = 'director/controlinternodir.html'
    permission_required = ''
    url_redirect = reverse_lazy('controldiariodir')
    anio = 2021
    mes = 1
    responsable = ''
    trabajo = ''
    pkresp = ''
    pktrabajo=''
    rango = 0
    director_id = 0
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def get_queryset(self):
        today = date.today()
        if self.kwargs['mes'] and int(self.kwargs['mes'])>0:
           rango = int(self.kwargs['rango'])  
           mes = self.kwargs['mes'] 
           anio = self.request.session['gestion'] 
        else:
            rango = 1    
            mes = today.month
            anio = self.request.session['gestion']         
        self.mes = mes
        self.anio = anio
        self.rango = rango
        director = Personal.objects.filter(usuario_id=self.request.user.id).first()             
        self.director_id = director.id
        print(self.director_id)
        return super(ControlDirectorListView, self).get_queryset().all()
    
    def post(self, request, *args, **kwargs):
        data = {}
        print("guardar")
        try:
            horas = request.POST.getlist('horas[]')
            proyectos=request.POST.getlist('proyecto[]')
            fechas=request.POST.getlist('fecha[]')
            etapas=request.POST.getlist('etapa[]')
            i = 0
            director = Personal.objects.filter(usuario_id=self.request.user.id).first()             
            
            for etapa in etapas:
                proyecto = Proyecto.objects.filter(id=proyectos[i]).first()
                etapa = Etapa.objects.filter(id=etapas[i]).first()                
                if horas[i]:
                   controldirector = ControlDirector.objects.filter(proyecto = proyecto, etapa = etapa, fecha=fechas[i], director_id=director.id).first()
                   if controldirector:
                      controldirector.horas=horas[i]
                   else:
                        controldirector = ControlDirector(proyecto = proyecto, etapa = etapa, fecha=fechas[i], horas=horas[i], director_id=director.id)
                   controldirector.save()
                i = i + 1
            return HttpResponseRedirect(reverse_lazy('controldiariodir', kwargs={'rango':1, 'mes': request.POST['mes'], 'anio': request.POST['anio'] }))
               
            
        except Exception as e:
            data['error1'] = str(e)
            print(str(e))
            return redirect(reverse_lazy('controldiariodir', kwargs={'rango':1, 'mes': request.POST['mes'], 'anio': request.POST['anio'] })) 

    def lista_meses(self):
        import datetime      
        array1 = []
        meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']; 
        array = []
        i = 0
        for mes1 in meses:      
            mesc = {
              'mes_str' : meses[i],
              'mes'     : str(i + 1)}
            i = i + 1
            array1.append(mesc)
        return array1

    def diasmes(self, mes, rango):
        dias_mes1 = [31, 28, 31, 30, 31, 30, 31, 31, 31, 31, 30, 31]
        if rango > 4:
           resto = dias_mes1[int(mes)-1] 
        else:
             resto = 7*rango
        return resto

    def lista(self):  
        import datetime      
        array1 = []
        dias_mes1 = [31, 28, 31, 30, 31, 30, 31, 31, 31, 31, 30, 31]
        resto = self.diasmes(int(self.mes), int(self.rango))
        dias_mes = [self.diasmes(1, int(self.rango)), self.diasmes(2, int(self.rango)), self.diasmes(3, int(self.rango)), self.diasmes(4, int(self.rango)), self.diasmes(5, int(self.rango)), self.diasmes(6, int(self.rango)), self.diasmes(7, int(self.rango)), self.diasmes(8, int(self.rango)), self.diasmes(9, int(self.rango)), self.diasmes(10, int(self.rango)), self.diasmes(11, int(self.rango)), self.diasmes(12, int(self.rango))]
        dias_semana = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];         
        for dia in range(int(self.rango-1)*7+1,dias_mes[int(self.mes) - 1] + 1):
            dia = datetime.datetime(int(self.anio), int(self.mes), dia)            
            if (dia.weekday()<=5):
               diasem = dias_semana[dia.weekday()+1][:2]
            else:
                diasem = dias_semana[0][:2]
            dia = {
               'dia_semana':diasem,
               'dia':dia.day}
            array1.append(dia)
        return array1
    
    def lista_semanas(self):  
        import datetime      
        array1 = []
        dias_semanas = ["01 - 07", "08 - 14", "15 - 21", "22 - 28", "29 - 31"]                         
        for rango1 in range(1, 6):            
            semana = {
               'idrango':str(rango1),
               'rango': dias_semanas[rango1 - 1]}
            array1.append(semana)
        return array1

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['dias'] = self.lista()
        context['action'] = 'searchdata'  
        context['responsable'] = self.responsable  
        mes_anio = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]   
        context['mes_str'] = mes_anio[int(self.mes) - 1]      
        context['anio_str'] = self.anio                 
        context['trabajo'] = self.trabajo
        self.mes_anio = mes_anio
        context['meses'] = self.lista_meses()
        context['pkresp'] = self.pkresp
        context['pktrabajo'] = self.pktrabajo
        context['mes'] = str(self.mes)
        context['horas'] = horasdia(self.request)
        context['etapas'] = Etapa.objects.all()
        context['proyectos'] = Proyecto.objects.all().order_by('nombre_proyecto')
        context['semanas'] = self.lista_semanas()        
        context['idrango'] = self.kwargs['rango']  
        context['desde'] = (int(self.rango) - 1)*7 + 1
        context['hasta'] = (int(self.rango) - 1)*7 + 8
        context['director_id'] = self.director_id

        return context


def cargardatosdir(request):
    xdirector_id = request.GET.get('director_id','')
    supervisor = Personal.objects.get(id=xdirector_id)
    xmes = request.GET.get('xmes','')
    xanio = request.GET.get('xanio','')
    dias_mes = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    desde = xanio + "-" + xmes + "-" + "01" 
    hasta = xanio + "-" + xmes + "-" + str(dias_mes[int(xmes) - 1])
    rango = [desde, hasta]
    controlessupervisor = ControlDirector.objects.filter(director_id=xdirector_id, fecha__range = rango).values('id','proyecto_id', 'etapa_id', 'fecha', 'horas') 
    array=[]
    dato = ()
    if controlessupervisor:
       for controlsupervisor in controlessupervisor:
           print(controlsupervisor)
           dato = {
                    'id': controlsupervisor['id'],
                    'proyecto_id': controlsupervisor['proyecto_id'],                                 
                    'etapa_id': controlsupervisor['etapa_id'],                                 
                    'fecha' : str(controlsupervisor['fecha']),
                    'horas' : controlsupervisor['horas']
                  }
           array.append(dato)
       return HttpResponse(  json.dumps(
                       array
                ), content_type = 'application/json')
    else:
        return HttpResponse(json.dumps(
                        [
                            {
                                'id': 0,
                                'proyecto__nombre_proyecto': "XXXXXXXX",                                 
                            },
                        ]
                ), content_type = 'application/json')

def reportemensualdirector(request):
    xmes = request.GET.get('mes','')
    meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
    xanio = request.GET.get('gestion','')
    reportemes=Reportes.objects.filter(id=5).first()
    report = ReporteControlDiarioDirector(reportemes, xmes, xanio, meses[int(xmes)-1])                  
    return report.render_to_response()
